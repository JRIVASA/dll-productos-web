VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsSeccionWeb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private mVarIndice              As Integer
Private mVarDescripcion             As String
Private mVarItems               As Dictionary
Private mUBound                 As Long

Property Get Indice() As Integer
    Indice = mVarIndice
End Property

Property Let Indice(ByVal Valor As Integer)
    mVarIndice = Valor
End Property

Property Get Descripcion() As String
    Descripcion = mVarDescripcion
End Property

Property Let Descripcion(ByVal Valor As String)
    mVarDescripcion = Valor
End Property

Property Get Items() As Dictionary
    Set Items = mVarItems
End Property

Property Let Items(Valor As Dictionary)
    Set mVarItems = Valor
End Property

Property Get UpperBound() As Long
    UpperBound = mUBound
End Property

Property Let UpperBound(ByVal Valor As Long)
    mUBound = Valor
End Property

Property Get ProximoID() As Long
    If mVarItems.Count > 0 Then
        Dim TmpHighestKey As Long
        For Each Key In AsEnumerable(mVarItems.Keys)
            If Key > TmpHighestKey Then
                TmpHighestKey = Key
            End If
        Next
        ProximoID = TmpHighestKey + 1
    Else
        ProximoID = 1
    End If
End Property

Private Sub Class_Initialize()
    Set mVarItems = New Dictionary
End Sub
