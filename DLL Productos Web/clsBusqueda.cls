VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsBusqueda"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Enum eOpCampoBusqueda
    opCampoStr
    opCampoBit
    opCampoNum
    opCampoFec
End Enum

Private Type Estruc_Busqueda
    sbEsCampoBusqueda                                               As Boolean
    sbCampoPred                                                     As Boolean
    sbCampoMostrar                                                  As String
    sbCampoTabla                                                    As String
    sbCampoRecordset                                                As String
    sbIdCampoBusqueda                                               As Integer
    sbCampoLongitud                                                 As Long
    sbCampoTipo                                                     As eOpCampoBusqueda
End Type


Private mvarConexion                                                As ADODB.Connection
Private defBusqueda()                                               As Estruc_Busqueda
Private mvarMultiSeleccion                                          As Boolean
Private mvarPresionoStop                                            As Boolean
Private mvarCodigoSel                                               As String
Private mvarIndiceCampoFecha                                        As Integer
Private mvarContador                                                As Integer
Private mvarTextoCmd                                                As String
Private mvarResultado                                               As Variant
Private mvarAutoBusqueda                                            As Boolean
Private mvarTamanoLetra                                             As Integer
Private mvarNumeroLineaAvance                                       As Integer
Private mvarTitulo                                                  As String



Event AvanceBusqueda(RegActual As Long, NumRegistros As Long)




'*********************************** Property Set de la Clase ***********************************************
Property Set ConexionBusqueda(pValor As Object)
    Set mvarConexion = pValor
End Property

Property Get ConexionBusqueda() As Object
    Set ConexionBusqueda = mvarConexion
End Property

'*********************************** Property Let de la Clase ***********************************************

Property Let ResultadoBusqueda(pValor)
    mvarResultado = pValor
End Property

Property Let MultiSeleccion(pValor As Boolean)
    mvarMultiSeleccion = pValor
End Property

Property Let IndiceCampoFecha(pValor As Integer)
    mvarIndiceCampoFecha = pValor
End Property

Property Let TextoComandoSql(pValor As String)
    mvarTextoCmd = pValor
End Property

Property Let PresionoStop(pValor As Boolean)
    mvarPresionoStop = pValor
End Property

Property Let CodigoSeleccionado(pValor As String)
    mvarCodigoSel = pValor
End Property

Property Let AutoBusqueda(pValor As Boolean)
    mvarAutoBusqueda = pValor
End Property

'*********************************** Property Get de la Clase ***********************************************

Property Get MultiSeleccion() As Boolean
    MultiSeleccion = mvarMultiSeleccion
End Property

Property Get BusquedaManejaFecha() As Boolean
    BusquedaManejaFecha = mvarManejaFecha
End Property

Property Get PresionoStop() As Boolean
    PresionoStop = mvarPresionoStop
End Property

Property Get TextoComandoSql() As String
    TextoComandoSql = mvarTextoCmd
End Property

Property Get IndiceCampoFecha() As Integer
    IndiceCampoFecha = mvarIndiceCampoFecha
End Property

Property Get ResultadoBusqueda()
    ResultadoBusqueda = mvarResultado
End Property

Property Get AutoBusqueda() As Boolean
    AutoBusqueda = mvarAutoBusqueda
End Property

Property Get TamanoLetra() As Integer
    TamanoLetra = mvarTamanoLetra
End Property

Property Get NumeroLineaAvance() As Integer
    NumeroLineaAvance = mvarNumeroLineaAvance
End Property

Property Get Titulo() As String
    Titulo = mvarTitulo
End Property


'*************************************************** Metodos de la Clase *****************************************

Public Sub AgregarCamposBusqueda(pCampoMostrar As String, pCampoTabla As String, pCampoRs As String, pLongitud As Long, Optional pesCampoFiltro As Boolean = False, Optional pTipoCampo As eOpCampoBusqueda = opCampoStr, Optional Predeterminado As Boolean = False)
    ReDim Preserve defBusqueda(mvarContador)
    defBusqueda(mvarContador).sbCampoMostrar = pCampoMostrar
    defBusqueda(mvarContador).sbCampoPred = Predeterminado
    defBusqueda(mvarContador).sbCampoTabla = pCampoTabla
    defBusqueda(mvarContador).sbCampoRecordset = pCampoRs
    defBusqueda(mvarContador).sbCampoLongitud = pLongitud
    defBusqueda(mvarContador).sbEsCampoBusqueda = pesCampoFiltro
    defBusqueda(mvarContador).sbCampoTipo = pTipoCampo
    If pesCampoFiltro And pTipoCampo = opCampoFec Then
        If IndiceCampoFecha = -1 Then IndiceCampoFecha = mvarContador
        mvarManejaFecha = True
    End If
    mvarContador = mvarContador + 1
End Sub

Public Function MostrarInterfazBusqueda(pTitulo As String, Optional AutoBusqueda As Boolean = False, Optional MultiSeleccion As Boolean, Optional NumLineasAvance As Integer = 7, Optional TamLetra As Integer = -1)
    With Frm_Super_Consultas
        Set .mClsBusqueda = Me
        mvarTitulo = pTitulo
        mvarAutoBusqueda = AutoBusqueda
        mvarMultiSeleccion = MultiSeleccion
        mvarNumeroLineaAvance = NumLineasAvance
        mvarTamanoLetra = TamLetra
        If mvarAutoBusqueda Then .txtDato.Text = "%"
        .Show vbModal
        MostrarInterfazBusqueda = mvarResultado
        Set Frm_Super_Consultas = Nothing
    End With
End Function

Friend Sub IniciarFrmBusqueda(ByRef pLv As ListView, ByRef pCbo As ComboBox)
    Dim mCampo As Integer
    Dim nIndex As Integer
    
    pLv.ListItems.Clear
    pLv.ColumnHeaders.Clear
    pLv.MultiSelect = MultiSeleccion: pLv.Checkboxes = MultiSeleccion
    pCbo.Clear
    For mCampo = 0 To mvarContador - 1
        pLv.ColumnHeaders.Add , , defBusqueda(mCampo).sbCampoMostrar, defBusqueda(mCampo).sbCampoLongitud
        If defBusqueda(mCampo).sbEsCampoBusqueda And defBusqueda(mCampo).sbCampoTipo <> opCampoFec Then
            pCbo.AddItem defBusqueda(mCampo).sbCampoMostrar, contador
            pCbo.ItemData(contador) = mCampo
            If defBusqueda(mCampo).sbCampoPred Then nIndex = contador
            contador = contador + 1
            
        End If
    Next mCampo
    pCbo.ListIndex = nIndex
        
End Sub

Friend Function BuscarFiltroConsulta(pCriterio, pDatoBuscar, pIndice As String, pFechaI As Date, pFechaF As Date)
    Dim mWhere As String
    
    mWhere = IIf(InStr(1, UCase(pCriterio), "WHERE") > 1, " and ", " where ")
    Select Case defBusqueda(pIndice).sbCampoTipo
       Case eOpCampoBusqueda.opCampoStr
            mWhere = mWhere & defBusqueda(pIndice).sbCampoTabla & " like '" & pDatoBuscar & "%'"
       
       Case eOpCampoBusqueda.opCampoNum, eOpCampoBusqueda.opCampoBit
            mWhere = mWhere & defBusqueda(pIndice).sbCampoTabla & "=" & pDatoBuscar
            
       Case Else
            mWhere = mWhere & defBusqueda(pIndice).sbCampoTabla & "='" & pDatoBuscar & "' "
        
    End Select
    BuscarFiltroConsulta = mWhere
            
End Function

Friend Sub LlenarLvBusqueda(pLv As ListView, pRs As ADODB.Recordset)
    Dim mItem As ListItem
    
    On Error GoTo Errores
    Do While Not pRs.EOF And Not PresionoStop
        DoEvents
        Set mItem = pLv.ListItems.Add(, , pRs.Fields(defBusqueda(0).sbCampoRecordset).Value)
        For I = 1 To mvarContador - 1
            Select Case defBusqueda(I).sbCampoTipo
                Case eOpCampoBusqueda.opCampoBit
                    mItem.SubItems(I) = IIf(pRs.Fields(defBusqueda(I).sbCampoRecordset).Value, "Si", "No")
                Case eOpCampoBusqueda.opCampoNum
                    mItem.SubItems(I) = FormatNumber(pRs.Fields(defBusqueda(I).sbCampoRecordset).Value, 2)
                Case eOpCampoBusqueda.opCampoFec
                    mItem.SubItems(I) = FormatDateTime(pRs.Fields(defBusqueda(I).sbCampoRecordset).Value, vbShortDate)
                Case Else
                    mItem.SubItems(I) = pRs.Fields(defBusqueda(I).sbCampoRecordset).Value
            End Select
        Next I
        
        RaiseEvent AvanceBusqueda(pRs.AbsolutePosition, pRs.RecordCount)
        pRs.MoveNext
    Loop
    PresionoStop = False
    Exit Sub
Errores:
    
    MensajeSistema Err.Description, False
    Err.Clear
    
            
End Sub

Friend Function BuscarCampoFecha() As String
    BuscarCampoFecha = defBusqueda(IndiceCampoFecha).sbCampoTabla
End Function

Friend Function TomarDatosSeleccionados(pLv As ListView)
    Dim mArrDatos As Variant
    Dim mCadena As String
    Dim mFila As Long, mContador As Long
    
    mContador = 0
    ReDim mArrDatos(mContador)
    If MultiSeleccion Then
        For mFila = 1 To pLv.ListItems.Count
            If pLv.ListItems(mFila).Checked Then
                ReDim Preserve mArrDatos(mContador)
                mArrDatos(mContador) = Split(TomarFilaLv(pLv, mFila), "|")
                mContador = mContador + 1
            End If
        Next mFila
    Else
        mArrDatos(mContador) = Split(TomarFilaLv(pLv, pLv.SelectedItem.Index), "|")
    End If
    TomarDatosSeleccionados = mArrDatos
        
End Function

Private Function TomarFilaLv(pLv As ListView, pFila As Long) As String
    Dim mCadena As String
    Dim mCol As Integer
    
    mCadena = pLv.ListItems(pFila).Text
    For mCol = 1 To pLv.ColumnHeaders.Count - 1
        mCadena = mCadena & "|" & pLv.ListItems(pFila).SubItems(mCol)
    Next mCol
    TomarFilaLv = mCadena
End Function

Friend Function ValidarSeleccion(pLv As ListView) As Boolean
    Dim mFila As Long
    Dim mFound As Boolean
    
    mFila = 0
    mFound = Not mvarMultiSeleccion
    If mvarMultiSeleccion Then
        While mFila < pLv.ListItems.Count And Not mFound
             mFila = mFila + 1
             mFound = pLv.ListItems(mFila).Checked
        Wend
    End If
    ValidarSeleccion = mFound
End Function


Friend Function MensajeSistema(Msg As String, BtnCancel As Boolean) As Boolean
    Dim cMensaje As clsMensajeria
    
    Set cMensaje = New clsMensajeria
    MensajeSistema = cMensaje.Mensaje(Msg, BtnCancel)
    Set cMensaje = Nothing
End Function

'*************************************************** Inicio Clase ********************************************
Private Sub Class_Initialize()
    ReDim defBusqueda(0)
    IndiceCampoFecha = -1
    
End Sub

Private Sub Class_Terminate()
    Set mvarConexion = Nothing
End Sub
