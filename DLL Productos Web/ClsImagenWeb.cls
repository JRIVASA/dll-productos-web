VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsImagenWeb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private mVarSeccion             As ClsSeccionWeb
Private mVarCodigo              As String
Private mVarOrden                  As Long
Private mVarDescri              As String
Private mVarImgOriginal         As Object
Private mVarRutaImg             As String
Private mVarLinkContenido          As String
Private mVarBytes                  As Long
Private mVarMIMEType               As String
Private mVarExtension              As String
Private mVarCambio              As Boolean

Property Get Seccion() As ClsSeccionWeb
    Set Seccion = mVarSeccion
End Property

Property Let Seccion(Valor As ClsSeccionWeb)
    Set mVarSeccion = Valor
End Property

Property Get Codigo() As String
    Codigo = mVarCodigo
End Property

Property Let Codigo(ByVal Valor As String)
    mVarCodigo = Valor
End Property

Property Get Orden() As Long
    Orden = mVarOrden
End Property

Property Let Orden(ByVal Valor As Long)
    mVarOrden = Valor
End Property

Property Get Descri() As String
    Descri = mVarDescri
End Property

Property Let Descri(ByVal Valor As String)
    mVarDescri = Valor
End Property

Property Get ImgOriginal() As Object
    Set ImgOriginal = mVarImgOriginal
End Property

Property Let ImgOriginal(Valor As Object)
    Set mVarImgOriginal = Valor
End Property

Property Get RutaImg() As String
    RutaImg = mVarRutaImg
End Property

Property Let RutaImg(ByVal Valor As String)
    mVarRutaImg = Valor
End Property

Property Get LinkContenido() As String
    LinkContenido = mVarLinkContenido
End Property

Property Let LinkContenido(ByVal Valor As String)
    mVarLinkContenido = Valor
End Property

Property Get Bytes() As Long
    Bytes = mVarBytes
End Property

Property Let Bytes(ByVal Valor As Long)
    mVarBytes = Valor
End Property

Property Get MIMEType() As String
    MIMEType = mVarMIMEType
End Property

Property Let MIMEType(ByVal Valor As String)
    mVarMIMEType = Valor
End Property

Property Get Extension() As String
    Extension = mVarExtension
End Property

Property Let Extension(ByVal Valor As String)
    mVarExtension = Valor
End Property

Property Get PoseeCambios() As Boolean
    PoseeCambios = mVarCambio
End Property

Property Let PoseeCambios(ByVal Valor As Boolean)
    mVarCambio = Valor
End Property

Public Function CorrelativoImagen(mConexion As ADODB.Connection, Campo As String, Optional Sumar As Boolean = True) As String
    
    On Error GoTo ErrCorrelativo
    
    Dim mRs As ADODB.Recordset
    Dim mSql As String
    
    mSql = "SELECT * FROM MA_CORRELATIVOS WHERE cu_Campo = '" & Campo & "'"
    
    Set mRs = New ADODB.Recordset
    
    mRs.Open mSql, mConexion, adOpenStatic, adLockOptimistic, adCmdText
    
    If Not mRs.EOF Then
        If Sumar Then mRs!nu_Valor = mRs!nu_Valor + 1
        CorrelativoImagen = Format(mRs!nu_Valor, mRs!cu_Formato)
        mRs.Update
    Else
        mRs.AddNew
            mRs!cu_Campo = Campo
            mRs!nu_Valor = IIf(Sumar, 1, 0)
            mRs!cu_Descripcion = "Codigo para Imagenes Web de Propůsito General"
            mRs!cu_Formato = "0000000000"
        mRs.Update
        CorrelativoImagen = Format(1, mRs!cu_Formato)
    End If
    
    Exit Function
    
ErrCorrelativo:
    
    CorrelativoImagen = vbNullString
    
End Function
