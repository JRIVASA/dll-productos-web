VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsConexion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private mConexion                                           As ADODB.Connection
Private mvarServidor                                        As String
Private mvarUsuario                                         As String
Private mvarClave                                           As String
Private mvarBD                                              As String
Private mvarCnnTO                                           As Integer
Private mvarCmdTO                                           As Integer
Private mvarProveedor                                       As String

Property Get Conexion() As ADODB.Connection
    Set Conexion = mConexion
End Property

Public Sub IniciarClase(Srv As String, Optional BD As String = "VAD20", Optional User As String = "SA", _
Optional Clave As String = "", Optional CnnTO As Integer = 15, Optional CmdTO As Integer = 30, _
Optional Proveedor As String = "SQLOLEDB.1")
    mvarServidor = Srv
    mvarUsuario = User
    mvarBD = BD
    mvarClave = Clave
    mvarCnnTO = CnnTO
    mvarCmdTO = CmdTO
    mvarProveedor = Proveedor
End Sub

Public Function ConectarBD() As Boolean
    On Error GoTo Errores
    Set mConexion = New ADODB.Connection
    mConexion.ConnectionTimeout = mvarCnnTO
    mConexion.Open CadenaConexion
    ConectarBD = True
    Exit Function
Errores:
    Mensaje Err.Description & ",Conectando " & mServidor, False
    Err.Clear
End Function

Public Function CadenaConexion() As String
    CadenaConexion = "Provider=" & mvarProveedor & ";Initial Catalog=" & mvarBD & ";Data Source=" & mvarServidor & ";" _
    & IIf(mvarUsuario = vbNullString Or mvarClave = vbNullString, _
    "Persist Security Info=False;User ID=" & mvarUsuario & ";", _
    "Persist Security Info=True;User ID=" & mvarUsuario & ";Password=" & mvarClave & ";")
End Function

Public Function ExisteCampoRs(Rs As ADODB.Recordset, Campo As String) As Boolean
    Dim mField As ADODB.Field
    
    For Each mField In Rs.Fields
        If StrComp(mField.Name, Campo) = 0 Then
            ExisteCampoRs = True
            Exit Function
        End If
    Next
End Function

Private Function Mensaje(Msg As String, Cancelar As Boolean) As Boolean
    Dim mMensaje  As clsMensajeria
    
    Set mMensaje = New clsMensajeria
    Mensaje = mMensaje.Mensaje(Msg, Cancelar)
    Set mMensaje = Nothing
End Function
