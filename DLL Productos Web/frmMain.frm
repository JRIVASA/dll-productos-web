VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   60
   ClientWidth     =   15330
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   2  'CenterScreen
   Begin VB.VScrollBar ScrollGrid 
      Height          =   6585
      LargeChange     =   10
      Left            =   14420
      TabIndex        =   30
      Top             =   3900
      Width           =   675
   End
   Begin VB.Frame FrameSelect 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   12600
      TabIndex        =   29
      Top             =   4080
      Visible         =   0   'False
      Width           =   1575
      Begin VB.Image CmdSelect 
         Height          =   300
         Left            =   480
         Picture         =   "frmMain.frx":0000
         Top             =   120
         Width           =   900
      End
   End
   Begin VB.Frame FrameTitulo 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   23
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Productos Web"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   25
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12555
         TabIndex        =   24
         Top             =   75
         Width           =   1815
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14640
         Picture         =   "frmMain.frx":0384
         Top             =   -30
         Width           =   480
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grid 
      CausesValidation=   0   'False
      Height          =   6585
      Left            =   120
      TabIndex        =   6
      Top             =   3900
      Width           =   14985
      _ExtentX        =   26432
      _ExtentY        =   11615
      _Version        =   393216
      Cols            =   5
      FixedCols       =   0
      RowHeightMin    =   400
      BackColor       =   16448250
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16448250
      GridColor       =   13421772
      Redraw          =   -1  'True
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      Enabled         =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   255
      Left            =   120
      TabIndex        =   20
      Top             =   10530
      Visible         =   0   'False
      Width           =   15120
      _ExtentX        =   26670
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   0
      Scrolling       =   1
   End
   Begin VB.Frame Frame4 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Caption         =   "Frame4"
      ForeColor       =   &H80000008&
      Height          =   3255
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   15120
      Begin VB.TextBox Codigo 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2115
         TabIndex        =   32
         Top             =   480
         Width           =   7905
      End
      Begin VB.ComboBox CmbActivos 
         Height          =   315
         ItemData        =   "frmMain.frx":2106
         Left            =   11040
         List            =   "frmMain.frx":2108
         Style           =   2  'Dropdown List
         TabIndex        =   28
         Top             =   480
         Width           =   1335
      End
      Begin VB.CommandButton Cancelar 
         Caption         =   "&Detener"
         Height          =   1035
         Left            =   11400
         Picture         =   "frmMain.frx":210A
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   2160
         Width           =   1140
      End
      Begin VB.CommandButton cmdTodos 
         Caption         =   "&Todos"
         Height          =   1035
         Left            =   12600
         Picture         =   "frmMain.frx":3E8C
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   2160
         Width           =   1140
      End
      Begin VB.CommandButton cmdGuardar 
         Caption         =   "&Guardar"
         Height          =   1035
         Left            =   13800
         Picture         =   "frmMain.frx":5C0E
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   2160
         Width           =   1140
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   1035
         Left            =   10200
         Picture         =   "frmMain.frx":7990
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   2160
         Width           =   1140
      End
      Begin VB.TextBox marca 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2115
         TabIndex        =   2
         Top             =   1425
         Width           =   7905
      End
      Begin VB.TextBox Producto 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2115
         TabIndex        =   1
         Top             =   960
         Width           =   7905
      End
      Begin VB.CommandButton Categoria 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   3345
         Picture         =   "frmMain.frx":9712
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   2775
         Width           =   360
      End
      Begin VB.CommandButton Categoria 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   3345
         Picture         =   "frmMain.frx":9F14
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   2325
         Width           =   360
      End
      Begin VB.TextBox grupo 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2115
         MaxLength       =   10
         TabIndex        =   4
         Top             =   2325
         Width           =   1185
      End
      Begin VB.TextBox subgrupo 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2115
         MaxLength       =   10
         TabIndex        =   5
         Top             =   2775
         Width           =   1185
      End
      Begin VB.CommandButton Categoria 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   3345
         Picture         =   "frmMain.frx":A716
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   1860
         Width           =   360
      End
      Begin VB.TextBox departamento 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2115
         MaxLength       =   10
         TabIndex        =   3
         Top             =   1860
         Width           =   1185
      End
      Begin VB.Label lblMultiCriterio 
         BackColor       =   &H00E2962E&
         BackStyle       =   0  'Transparent
         Caption         =   $"frmMain.frx":AF18
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   1215
         Left            =   10320
         TabIndex        =   33
         Top             =   840
         Width           =   3975
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblCodigo 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   240
         TabIndex        =   31
         Top             =   480
         Width           =   660
      End
      Begin VB.Label lblActivo 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         Caption         =   "Activo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   10320
         TabIndex        =   27
         Top             =   480
         Width           =   585
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   2280
         X2              =   14710
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label Label2 
         BackColor       =   &H00E2962E&
         BackStyle       =   0  'Transparent
         Caption         =   "Criterios de Busqueda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   0
         Width           =   2655
      End
      Begin VB.Label lbl_departamento 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3765
         TabIndex        =   17
         Top             =   1860
         Width           =   6045
      End
      Begin VB.Label lbl_grupo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3765
         TabIndex        =   16
         Top             =   2325
         Width           =   6045
      End
      Begin VB.Label lbl_subgrupo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3765
         TabIndex        =   15
         Top             =   2775
         Width           =   6045
      End
      Begin VB.Label C�digo 
         BackStyle       =   0  'Transparent
         Caption         =   "SubGrupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   14
         Top             =   2775
         Width           =   1215
      End
      Begin VB.Label C�digo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Departamento *"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   2
         Left            =   240
         TabIndex        =   13
         Top             =   1860
         Width           =   1395
      End
      Begin VB.Label C�digo 
         BackStyle       =   0  'Transparent
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   3
         Left            =   240
         TabIndex        =   12
         Top             =   2325
         Width           =   1335
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         Caption         =   "Marca"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   1
         Left            =   240
         TabIndex        =   11
         Top             =   1455
         Width           =   525
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Index           =   0
         Left            =   240
         TabIndex        =   10
         Top             =   975
         Width           =   975
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public fCls As clsProductosWeb
Private mEjecutando As Boolean
Private mCancelar As Boolean
Private Band As Boolean
Private mRowCell As Long, mColCell As Long, Fila As Long
Private OriginalDescColWidth As Long

Private Sub FrameTitulo_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbLeftButton Then MoverVentana Me.hWnd
End Sub

Private Sub lbl_Organizacion_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    FrameTitulo_MouseMove Button, Shift, x, y
End Sub

Private Sub lbl_website_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    FrameTitulo_MouseMove Button, Shift, x, y
End Sub

Private Sub cancelar_Click()
    mCancelar = True
End Sub

Private Sub Categoria_Click(Index As Integer)
    
    Dim mBus As clsBusqueda
    Dim mTitulo As String, mSql As String
    Dim mText As TextBox, mLabel As Label
    Dim clsRutinas As clsMensajeria
    
    Set clsRutinas = New clsMensajeria
    
    If Index = 0 Then
        mSql = " SELECT * FROM MA_DEPARTAMENTOS "
        Me.grupo = ""
        Me.subgrupo = ""
        Set mText = departamento
        Set mLabel = lbl_departamento
        mTitulo = "D E P A R T A M E N T O"
    ElseIf Index = 1 Then
        If Trim(Me.departamento) = "" Then
            Call clsRutinas.Mensaje("Debe seleccionar un departamento.", False)
            Exit Sub
        End If
        mSql = " SELECT * FROM MA_GRUPOS WHERE c_Departamento = '" & Me.departamento & "'"
        Me.subgrupo = ""
        Set mText = grupo
        Set mLabel = lbl_grupo
        mTitulo = "G R U P O"
    ElseIf Index = 2 Then
        If Trim(Me.departamento) = "" Or Trim(Me.grupo) = "" Then
            Call clsRutinas.Mensaje("Debe seleccionar un departamento y un grupo.", False)
            Exit Sub
        End If
        mSql = " SELECT * FROM MA_SUBGRUPOS WHERE c_In_Grupo = '" & Me.grupo & "' AND c_In_Departamento = '" & Me.departamento & "'"
        Set mText = subgrupo
        Set mLabel = lbl_subgrupo
        mTitulo = "S U B G R U P O"
    End If
    
    If fCls.ClsCn.ConectarBD Then
        Set mBus = New clsBusqueda
        Set mBus.ConexionBusqueda = fCls.ClsCn.Conexion
        mBus.TextoComandoSql = mSql
        mBus.AgregarCamposBusqueda "Codigo", "c_codigo", "c_codigo", 3000, True, opCampoStr
        mBus.AgregarCamposBusqueda "Descripcion", "c_DESCRIPCIO", "c_DESCRIPCIO", 8080, True, opCampoStr, True
        mBus.MostrarInterfazBusqueda mTitulo, True, False
        If Not IsEmpty(mBus.ResultadoBusqueda) Then
            mText.Text = mBus.ResultadoBusqueda(0)(0)
            mLabel.Caption = mBus.ResultadoBusqueda(0)(1)
        End If
    End If

End Sub

Private Sub CmdBuscar_Click()
    
    Dim Rs As ADODB.Recordset
    Dim I As Long, mCriterio As String, mJoin As String
    
    On Error GoTo Errores
    
    If mEjecutando Then Exit Sub
    mEjecutando = True
    
    IniciarGrid
    
    If Not fCls.ClsCn.ConectarBD Then Exit Sub
    
    If Me.departamento.Text <> Empty Then
        
        mCriterio = " WHERE p.c_Departamento = '" & Me.departamento.Text & "'"
        
        If Me.grupo.Text <> "" Then
            
            mCriterio = mCriterio & " AND p.c_Grupo = '" & Me.grupo.Text & "'"
            
            If Me.subgrupo.Text <> "" Then
                
                mCriterio = mCriterio & " AND p.c_Subgrupo = '" & Me.subgrupo.Text & "'"
                
            End If
            
        End If
        
    End If
    
    If Trim(Codigo.Text) <> Empty Then
        
        mJoin = mJoin & " LEFT JOIN MA_CODIGOS C ON P.c_Codigo = C.c_CodNasa "
        
        If Codigo.Text Like "*;*" Then
            
            Dim mInCodigos As String, mCodigos, mCodigo
            
            mCodigos = Split(Codigo.Text, ";", , vbTextCompare)
            
            For Each mCodigo In AsEnumerable(mCodigos)
                mInCodigos = mInCodigos & ", '" & mCodigo & "'"
            Next
            
            mInCodigos = Mid(mInCodigos, 3)
            
            mCriterio = mCriterio & IIf(mCriterio <> "", " AND ", " WHERE ") & " (C.c_Codigo IN (" & mInCodigos & ")) "
            
        ElseIf Codigo.Text Like "*|*" Then
            
            mCodigos = Split(Codigo.Text, "|", , vbTextCompare)
            
            For Each mCodigo In AsEnumerable(mCodigos)
                mInCodigos = mInCodigos & ", '" & mCodigo & "'"
            Next
            
            mInCodigos = Mid(mInCodigos, 3)
            
            mCriterio = mCriterio & IIf(mCriterio <> "", " AND ", " WHERE ") & " (C.c_Codigo IN (" & mInCodigos & ")) "
            
        Else
            mCriterio = mCriterio & IIf(mCriterio <> "", " AND ", " WHERE ") & " (C.c_Codigo LIKE  '" & Codigo.Text & "') "
        End If
        
    End If
    
    If Trim(Producto.Text) <> Empty Then
        
        If Producto.Text Like "*|*" Then
            
            Dim mInDescri As String, mDescs, mDescri
            
            mDescs = Split(Producto.Text, "|", , vbTextCompare)
            
            For Each mDescri In AsEnumerable(mDescs)
                mInDescri = mInDescri & "OR c_Descri LIKE '" & mDescri & "'"
            Next
            
            mInDescri = Mid(mInDescri, 4)
            
            mCriterio = mCriterio & IIf(mCriterio <> "", " AND ", " WHERE ") & " (" & mInDescri & ") "
            
        Else
            mCriterio = mCriterio & IIf(mCriterio <> "", " AND ", " WHERE ") & " (c_Descri LIKE  '" & Producto.Text & "%') "
        End If
        
    End If
    
    If Trim(marca.Text) <> Empty Then
        
        If marca.Text Like "*|*" Then
            
            Dim mInMarcas As String, mMarcas, mMarca
            
            mMarcas = Split(marca.Text, "|", , vbTextCompare)
            
            For Each mMarca In AsEnumerable(mMarcas)
                mInMarcas = mInMarcas & "OR c_Marca LIKE '" & mMarca & "'"
            Next
            
            mInMarcas = Mid(mInMarcas, 4)
            
            mCriterio = mCriterio & IIf(mCriterio <> "", " AND ", " WHERE ") & " (" & mInMarcas & ") "
            
        Else
            mCriterio = mCriterio & IIf(Len(mCriterio) > 0, " AND ", " WHERE ") & "(c_Marca LIKE '" & marca.Text & "%') "
        End If
        
    End If
    
    If CmbActivos.ListIndex <> 0 Then
        mCriterio = mCriterio & IIf(Len(mCriterio) > 0, " AND ", " WHERE ") & "(n_Activo = " & IIf(CmbActivos.ListIndex = 1, 1, 0) & ") "
    End If
    
    SQL = "SELECT P.c_Codigo, c_Descri, W.Codigo " & _
    "FROM MA_PRODUCTOS P " & mJoin & " " & _
    "LEFT JOIN  " & fCls.TablaProductoWeb & " " & _
    "W ON P.c_Codigo = W.Codigo " & mCriterio
    
    Set Rs = New ADODB.Recordset
    
    Rs.CursorLocation = adUseClient
    Rs.Open SQL, fCls.ClsCn.Conexion, adOpenForwardOnly, adLockReadOnly
    
    ScrollGrid.Visible = False: FrameSelect.Visible = False: Grid.ScrollBars = flexScrollBarVertical
    
    If Not Rs.EOF Then
        
        mCancelar = False
        
        IniciarBarra Rs.RecordCount
        
        ScrollGrid.Min = 0
        ScrollGrid.Max = ValidarNumeroIntervalo(Rs.RecordCount, 32767, 0)
        ScrollGrid.Value = 0
        
        Grid.Visible = False
        
        While Not Rs.EOF And Not mCancelar
            
            DoEvents
            
            I = I + 1
            
            AvanceBarra I
            
            With Grid
                If I > .Rows - 1 Then .Rows = .Rows + 1
                .Row = I
                .Col = 0
                .Text = Rs!c_Codigo
                .Col = 1
                .Text = Rs!c_Descri
                .Col = 2
                .Text = IIf(IsNull(Rs!Codigo), "No", "Si")
                .RowData(I) = IIf(IsNull(Rs!Codigo), 0, 1)
            End With
            
            Rs.MoveNext
            
        Wend
        
        mEjecutando = False
        
Limite:
        
        ScrollGrid.Max = Grid.Rows - 1
        
        Grid.Row = 1
        
        If Grid.Rows > 8 Then
            ScrollGrid.Visible = True
            Grid.ScrollBars = flexScrollBarBoth
            
            Grid.ColWidth(1) = OriginalDescColWidth - ScrollGrid.Width
        Else
            Grid.ScrollBars = flexScrollBarNone
            ScrollGrid.Visible = False
            
            Grid.ColWidth(1) = OriginalDescColWidth
        End If
        
        Grid.Visible = True
        
        OcultarBarra
        
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
        
        Fila = 0
        grid_EnterCell
        
    Else
        Grid.Col = Grid.Cols - 1
        Grid.ColSel = Grid.Col
        fCls.Mensaje "No hay Productos con estas caracteristicas.", False
        mEjecutando = False
    End If
    
    Fila = 0
    
    cmdTodos.Caption = "&Todos"
    
    Grid.Visible = True
    
    Exit Sub
    
Errores:
    
    Grid.Visible = True
    
    'Resume ' Debug
    
    mEjecutando = False
    
    If Err.Number = 6 Then
        fCls.Mensaje "Se ha alcanzado el l�mite m�ximo de filas [32767]. Si desea ver los registros restantes debe realizar una consulta mas espec�fica.", False
        Resume Limite
    Else
        fCls.Mensaje Err.Description, False
    End If
    
    OcultarBarra
    
End Sub

Private Sub cmdGuardar_Click()
    
    Dim mSql As String
    Dim mSel As String
    
    If fCls.ClsCn.ConectarBD Then
        
        On Error GoTo Errores
        
        fCls.ClsCn.Conexion.BeginTrans
        
        For I = 1 To Grid.Rows - 1
            mSel = Grid.TextMatrix(I, 2)
            If StrComp(mSel, "Si", vbTextCompare) = 0 And Grid.RowData(I) = 0 Then
                
                ' si va a la tabla y no estaba ya
                mSql = "Insert into " & fCls.TablaProductoWeb & " (Codigo) " _
                    & "values ('" & Grid.TextMatrix(I, 0) & "')"
                
                fCls.ClsCn.Conexion.Execute mSql, Reg
                
                mSql = "Insert into " & fCls.TablaProductoWebPendiente & " (Codigo, TipoCambio) " _
                    & "values ('" & Grid.TextMatrix(I, 0) & "', 0)"
                
                fCls.ClsCn.Conexion.Execute mSql, Reg
                
                fCls.AgregarPendienteProd fCls.ClsCn.Conexion, fCls.ObtenerRsProducto(fCls.ClsCn.Conexion, Grid.TextMatrix(I, 0))
                fCls.AgregarPendienteCodigos fCls.ClsCn.Conexion, Grid.TextMatrix(I, 0)
                
            ElseIf StrComp(mSel, "No", vbTextCompare) = 0 And Grid.RowData(I) = 1 Then
                
                ' si no va a la tabla pero estaba
                mSql = "Delete from " & fCls.TablaProductoWeb & " where (codigo='" & Grid.TextMatrix(I, 0) & "')"
                
                fCls.ClsCn.Conexion.Execute mSql, Reg
                
                mSql = "Insert into " & fCls.TablaProductoWebPendiente & " (Codigo, TipoCambio) " _
                    & "values ('" & Grid.TextMatrix(I, 0) & "', 1)"
                    
                fCls.ClsCn.Conexion.Execute mSql, Reg
                
                'aqui insert into tabla cambios productos web para eliminarlo de la pagina
                'mSQl = ""
                'fCls.ClsCn.Conexion.Execute mSQl, Reg
            End If
        Next I
        
        fCls.ClsCn.Conexion.CommitTrans
        IniciarGrid
        
    End If
    
    Exit Sub
    
Errores:
    
    fCls.ClsCn.Conexion.RollbackTrans
    fCls.Mensaje Err.Description, False
    Err.Clear
    
End Sub

Private Sub CmdSelect_Click()
    Set FrmProductoImagenes.fCls = fCls
    FrmProductoImagenes.Producto = Grid.TextMatrix(Grid.Row, 0)
    FrmProductoImagenes.Show vbModal
End Sub

Private Sub cmdTodos_Click()
    
    Dim I As Long, Marcado As Boolean, Estatus As String
    
    Estatus = cmdTodos.Caption
    
    Marcado = (StrComp(Estatus, "&Todos", vbTextCompare) = 0) ' Si dice Todos, entonces Marcar
    
    For I = 1 To Grid.Rows - 1
        'MarcarDesmarcar i
        AsignarEstado I, Marcado ' Asignar nuevo estatus a toda la selecci�n.
    Next I
    
    cmdTodos.Caption = IIf(Marcado, "&Ninguno", "&Todos") ' Alternar proximo cambio de estatus.
    
End Sub

Private Sub departamento_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
       Categoria_Click 0
    End If
End Sub

Private Sub departamento_LostFocus()
    Dim Rs As ADODB.Recordset
    
    If Trim(Me.departamento) <> "" And fCls.ClsCn.ConectarBD Then
        SQL = " SELECT * FROM MA_DEPARTAMENTOS WHERE (c_Codigo = '" & Me.departamento & "')"
        Set Rs = New ADODB.Recordset
        Rs.Open SQL, fCls.ClsCn.Conexion, adOpenForwardOnly, adLockReadOnly
        If Rs.EOF Then
            fCls.Mensaje "Este c�digo no existe.", False
            Me.departamento = ""
            Me.departamento.SetFocus
        Else
            Me.lbl_departamento = Rs("c_descripcio")
        End If
        Rs.Close
    Else
        Me.lbl_departamento.Caption = ""
    End If
    Me.grupo = "": lbl_grupo.Caption = ""
    Me.subgrupo = "": lbl_subgrupo.Caption = ""
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        If Not ActiveControl Is Nothing Then
            If ActiveControl.Name = Grid.Name Then Exit Sub
        End If
        SendKeys Chr(vbKeyTab)
    End If
End Sub

Private Sub Form_Load()
    
    IniciarGrid
    
    CmbActivos.AddItem "Todos", 0
    CmbActivos.AddItem "Si", 1
    CmbActivos.AddItem "No", 2
    CmbActivos.ListIndex = 1
    cmdTodos.Caption = "&Todos"
    
    lblMultiCriterio.Caption = _
    "Nota: Si desea buscar por multiples criterios, separe los elementos por el caracter |           Ejemplo C�digos: 000001|7591811230058      Ejemplo con Descripciones / Marcas: %Stellar%|BUSINESS|Otro%"
    ' English:
    'Note: If you want to look for multiple items, split criteria by the character |               Codes Example: 000001|7591811230058        Item Names / Brands Example: %Stellar%|BUSINESS|Other%
    
    ' Traducir ' StellarMensaje()
    
End Sub

Private Sub FrameSelect_DragDrop(Source As Control, x As Single, y As Single)
    CmdSelect_Click
End Sub

Private Sub GRID_DblClick()
    grid_KeyDown vbKeyReturn, 0
End Sub

Private Sub Grid_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    grid_EnterCell
End Sub

Private Sub Grid_SelChange()
    grid_EnterCell
End Sub

Private Sub grid_EnterCell()
    
    On Error Resume Next
    
    If mEjecutando Then Exit Sub
    
    If Grid.Rows = 1 Then
        FrameSelect.Visible = False
        Exit Sub
    End If
    
    If Not Band Then
        
        If Fila <> Grid.Row Then
            
            If Fila > 0 Then
                Grid.RowHeight(Fila) = 600
            End If
            
            Grid.RowHeight(Grid.Row) = 720
            Fila = Grid.Row
            
        End If
    
        MostrarEditorTexto2 Me, Grid, CmdSelect, mRowCell, mColCell
        Grid.ColSel = 0
        
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    Else
        Band = False
    End If
    
End Sub

Public Sub MostrarEditorTexto2(pFrm As Form, pGrd As Object, ByRef txteditor As Object, ByRef cellRow As Long, ByRef cellCol As Long, Optional pResp As Boolean = False)
    
    On Error Resume Next
    
    With pGrd

        .RowSel = .Row
        
        If .Col <> 3 Then Band = True
        .Col = 3
        
        FrameSelect.BackColor = pGrd.BackColorSel
        FrameSelect.Move .Left + .CellLeft, .Top + .CellTop, .CellWidth, .CellHeight
        CmdSelect.Move ((FrameSelect.Width / 2) - (CmdSelect.Width / 2)), ((FrameSelect.Height / 2) - (CmdSelect.Height / 2))
        FrameSelect.Visible = True: CmdSelect.Visible = True
        FrameSelect.ZOrder
        
        cellRow = .Row
        cellCol = .Col

     End With
     
End Sub

Private Sub grid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn And Grid.Row > 0 Then
        MarcarDesmarcar Grid.Row
    End If
End Sub

Private Sub grupo_KeyDown(KeyCode As Integer, Shift As Integer)
      If KeyCode = vbKeyF2 Then
        If Trim(departamento.Text) <> "" Then
            Categoria_Click 1
        End If
    End If
End Sub

Private Sub grupo_LostFocus()
    Dim Rs As ADODB.Recordset
    
    If Trim(Me.departamento) = "" And Trim(grupo) <> "" Then
        fCls.Mensaje "debe escojer un departamento", False
        Exit Sub
    End If
    
    If Trim(Me.grupo) <> "" And fCls.ClsCn.ConectarBD Then
        SQL = " SELECT * From MA_grupos WHERE (C_CODIGO = '" & Me.grupo & "') and c_departamento= '" & Me.departamento & "'"
        
        Set Rs = New ADODB.Recordset
        Rs.Open SQL, fCls.ClsCn.Conexion, adOpenForwardOnly, adLockReadOnly
        
        If Rs.EOF Then
            fCls.Mensaje "Este c�digo no existe para este departamento.", False
            Me.grupo.SetFocus
            Me.grupo = ""
        Else
            Me.lbl_grupo = Rs("c_descripcio")
        End If
        
        Rs.Close
        
    Else
        Me.lbl_grupo.Caption = ""
        grupo.Text = ""
    End If
    
    subgrupo = ""
    lbl_subgrupo.Caption = ""
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    'Debug.Print ScrollGrid.value
    ScrollGrid_Change
End Sub

Private Sub subgrupo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
        If Trim(departamento.Text) <> "" And Trim(grupo.Text) <> "" Then
            Categoria_Click 2
        End If
    End If
End Sub

Private Sub subgrupo_LostFocus()
    Dim Rs As ADODB.Recordset
    
    If Trim(departamento) = "" Or Trim(grupo) = "" And Trim(subgrupo) <> "" Then
        fCls.Mensaje "Debe escojer un departamento y grupo.", False
        Exit Sub
    End If
    
    If Trim(Me.subgrupo) <> "" And fCls.ClsCn.ConectarBD Then
        SQL = " SELECT * From MA_subgrupos WHERE (C_CODIGO = '" & Me.subgrupo & "') and c_in_departamento= '" & Me.departamento & "' and c_in_grupo= '" & Me.grupo & "'"
        Set Rs = New ADODB.Recordset
        Rs.Open SQL, fCls.ClsCn.Conexion, adOpenForwardOnly, adLockReadOnly
        
        If Rs.EOF Then
            fCls.Mensaje "Este c�digo no existe para este departamento y grupo.", False
            Me.subgrupo.SetFocus
            Me.subgrupo = ""
        Else
            Me.lbl_subgrupo = Rs("c_descripcio")
        End If
        
        Rs.Close
    Else
        subgrupo.Text = ""
        lbl_subgrupo.Caption = ""
    End If

End Sub

Private Sub IniciarGrid()
    With Me.Grid
        .Clear
        .Cols = 5
        .Rows = 2 '2
        .SelectionMode = flexSelectionByRow
        .FixedCols = 0
        .FixedRows = 1
        .Rows = 1
        .RowHeight(0) = 425
        .RowHeightMin = 600 '.RowHeight(0) * 2
        '.RowData(1) = 0
        OriginalDescColWidth = 10950
        EncabezadoCol 0, 2000, "Codigo", flexAlignCenterCenter, True
        EncabezadoCol 1, 0, "", flexAlignLeftCenter, True ' Cargar primero el ColWidth de la Columna
        EncabezadoCol 1, OriginalDescColWidth, "Descripcion", flexAlignCenterCenter, False ' Colocar las propiedades reales del cabecero.
        EncabezadoCol 2, 1000, "Web", flexAlignCenterCenter, True
        EncabezadoCol 3, 1000, "Imagen", flexAlignCenterCenter, True
        .ColWidth(4) = 0 ' Columna sin datos para ocultar la seleccion de columnas en la fila de cabecero fija (0) para que no le cambie el color.
        .ScrollTrack = True
        .Row = 0
        .Col = 4
    End With
End Sub

Private Sub EncabezadoCol(Col As Integer, ByVal Ancho As Integer, ByVal Descri As String, Aling As AlignmentSettings, TodaCol As Boolean)
    Grid.Col = Col
    Grid.ColWidth(Col) = Ancho
    Grid.Row = 0
    Grid.Text = Descri
    If Not TodaCol Then
        Grid.CellAlignment = Aling
    Else
        Grid.ColAlignment(Col) = Aling
    End If
    'GRID.CellFontBold = True
    
End Sub

Private Sub AvanceBarra(Reg As Long)
    ProgressBar1.Value = Reg
End Sub

Private Sub IniciarBarra(NumReg As Long)
    ProgressBar1.Min = 0
    ProgressBar1.Value = 0
    ProgressBar1.Max = NumReg + 1
    ProgressBar1.Visible = True
End Sub

Private Sub OcultarBarra()
    ProgressBar1.Visible = False
End Sub

Private Sub MarcarDesmarcar(Fila As Long)
    
    Dim Valor As String
    
    Valor = Grid.TextMatrix(Fila, 2)
    
    If Trim(Valor) <> "" Then
        Grid.TextMatrix(Fila, 2) = IIf(StrComp(Valor, "SI", vbTextCompare) = 0, "No", "Si")
    End If
    
End Sub

Private Sub AsignarEstado(Fila As Long, Marcado As Boolean)
    
    Dim Valor As String
    
    Valor = IIf(Marcado, "Si", "No")
    Grid.TextMatrix(Fila, 2) = Valor
    
End Sub
