VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form frm_SeleccionCombo 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   1515
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   4965
   ControlBox      =   0   'False
   Icon            =   "frm_SeleccionCombo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1515
   ScaleWidth      =   4965
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox CmdSalir 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   495
      Left            =   4050
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   750
      Width           =   705
   End
   Begin VB.ComboBox CmbSeleccion 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   555
      Left            =   250
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   720
      Width           =   3600
   End
   Begin VB.Frame Frame5 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   12120
      Begin VB.Label lbl_Organizacion 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Selecci�n de Reporte"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   75
         Width           =   4455
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   7275
         TabIndex        =   4
         Top             =   75
         Width           =   1935
      End
   End
   Begin MSFlexGridLib.MSFlexGrid GridEvitarFoco 
      Height          =   30
      Left            =   2160
      TabIndex        =   0
      Top             =   720
      Width           =   135
      _ExtentX        =   238
      _ExtentY        =   53
      _Version        =   393216
   End
End
Attribute VB_Name = "frm_SeleccionCombo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmbSeleccion_DblClick()
    CmdSalir_Click
End Sub

Private Sub CmbSeleccion_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then CmdSalir_Click
End Sub

Private Sub CmdSalir_Click()

' No la indento debido a que esto solo es una l�nea de control
' Para que se comporte como un bot�n regular.
' El c�digo del metodo es lo que hay adentro.
If CmdSalir.Value = vbChecked Then

    CmdSalir.Value = vbUnchecked ' L�nea de Control.
    GridEvitarFoco.SetFocus ' L�nea de Control.
    
    Unload Me
    
End If

End Sub

Private Sub Form_Activate()
    'GridEvitarFoco.SetFocus
End Sub

Private Sub Form_Load()
    lbl_Organizacion.Caption = "Selecci�n" 'seleccion de reporte
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Me.Visible Then
        Cancel = 1
        Me.Hide
    Else
        Cancel = 0
        'Unload
    End If
End Sub
