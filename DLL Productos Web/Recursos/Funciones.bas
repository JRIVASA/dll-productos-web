Attribute VB_Name = "Funciones"
Public Enum FechaBDPrecision
    FBD_Fecha
    FBD_FechaYMinuto
    FBD_FULL
    FBD_HoraYMinuto
    FBD_HoraFull
End Enum

Public Enum FindFileConstants
    NO_SEARCH_MUST_FIND_EXACT_MATCH = -1
    SEARCH_ALL_UPPER_LEVELS = 0
    SEARCH_1_UPPER_LEVEL = 1
    SEARCH_2_UPPER_LEVELS = 2
    SEARCH_3_UPPER_LEVELS = 3
    SEARCH_N_INPUT_ANY_NUMBER = 4
    '...
End Enum

Public DBNull
Public LcCodUsuario As String

Public Const SoftwarePath = "BIGWISE\Stellar\BUSINESS"
Public GlobalEventTimer As SelfTimer

Public Function PuedeObtenerFoco(Objeto As Object, Optional OrMode As Boolean = False) As Boolean
    If Not Objeto Is Nothing Then
        If OrMode Then
            PuedeObtenerFoco = Objeto.Enabled Or Objeto.Visible
        Else
            PuedeObtenerFoco = Objeto.Enabled And Objeto.Visible
        End If
    End If
End Function

Public Function ValidarNumeroIntervalo(ByVal pValor, Optional pMax As Variant, Optional pMin As Variant) As Variant

    On Error GoTo Err

    If Not IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor > pMax Then
            pValor = pMax
        ElseIf pValor < pMin Then
            pValor = pMin
        End If
    ElseIf Not IsMissing(pMax) And IsMissing(pMin) Then
        If pValor > pMax Then pValor = pMax
    ElseIf IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor < pMin Then pValor = pMin
    End If
    
    ValidarNumeroIntervalo = pValor
    
    Exit Function
    
Err:

    ValidarNumeroIntervalo = pValor

End Function

Public Function PopImageOfDb(ByRef ObjImg As Object, RecSet As ADODB.Recordset, Campo As Variant, _
Optional ByVal TmpImgFileName As Variant) As Boolean
    
    On Error GoTo ErrLoad
    
    Dim LibreArc As Integer
    Dim FS, f, s, counter As Long
    
    PopImageOfDb = False
    'VERIFICA SI EXISTE PARA BORRARLO ANTES DE EMPEZAR
    
    If IsMissing(TmpImgFileName) Then
        sTemp = "STELLAR_BK001.TMP"
    Else
        sTemp = TmpImgFileName
    End If
    
    Set FS = CreateObject("Scripting.FileSystemObject")
    
    If FS.FileExists(sTemp) Then
        Set f = FS.GetFile(sTemp)
        f.Delete
    End If
    
    LibreArc = FreeFile()
    
    Open sTemp For Append Access Write As #LibreArc
    
    If Not RecSet.EOF Then
        Buffer = RecSet.Fields(Campo).GetChunk(RecSet.Fields(Campo).ActualSize)
    End If
    
    Print #LibreArc, Buffer
    
    Close LibreArc
    
    ObjImg.Picture = LoadPicture(sTemp)
    
    'VERIFICA SI EXISTE PARA BORRARLO AL TERMINAR
    
    If FS.FileExists(sTemp) And IsMissing(TmpImgFileName) Then
        Set f = FS.GetFile(sTemp)
        f.Delete
    End If
    
    PopImageOfDb = True
    
    Exit Function
    
ErrLoad:
    
    Close LibreArc
    
    ObjImg.Picture = LoadPicture
    
End Function

Public Function PopStreamOfDb(ByRef ObjImg As Object, RecSet As ADODB.Recordset, Campo As Variant, _
Optional ByVal TmpImgFileName As Variant) As Boolean
    
    On Error GoTo ErrLoad
    
    Dim LibreArc As Integer
    Dim FS, f, s, counter As Long
    
    PopStreamOfDb = False
    
    'VERIFICA SI EXISTE PARA BORRARLO ANTES DE EMPEZAR
    
    If IsMissing(TmpImgFileName) Then
        sTemp = "STELLAR_BK001.TMP"
    Else
        sTemp = TmpImgFileName
    End If
    
    Set FS = CreateObject("Scripting.FileSystemObject")
    
    'If FS.FileExists(sTemp) Then
        'Set f = FS.GetFile(sTemp)
        'f.Delete
    'End If
    
    KillSecure sTemp
    
    If Not RecSet.EOF Then
        
        Dim TmpFileStream As ADODB.Stream
        
        Set TmpFileStream = New ADODB.Stream
        
        TmpFileStream.Type = adTypeBinary
        TmpFileStream.Open
        TmpFileStream.Write RecSet.Fields(Campo).Value
        
        TmpFileStream.SaveToFile sTemp, adSaveCreateOverWrite
        TmpFileStream.Close
        
    End If
    
    ObjImg.Picture = LoadPicture(sTemp)
    
    'VERIFICA SI EXISTE PARA BORRARLO AL TERMINAR
    
    If FS.FileExists(sTemp) And IsMissing(TmpImgFileName) Then
        Set f = FS.GetFile(sTemp)
        f.Delete
    End If
    
    PopStreamOfDb = True
    
    Exit Function
    
ErrLoad:
    
    ObjImg.Picture = LoadPicture
    
End Function

Public Function PopFileStream(FilePath As String, Optional RecSet, Optional Campo) As Object
    
    On Error GoTo Errores
    
    Dim m_Stream As ADODB.Stream, sTemp
    
    sTemp = FilePath
    
    Set FS = CreateObject("Scripting.FileSystemObject")
    
    Set m_Stream = New ADODB.Stream
    
    If FS.FileExists(sTemp) Then
        
        m_Stream.Type = adTypeBinary
        m_Stream.Open
        m_Stream.LoadFromFile sTemp
        
    End If

    Set PopFileStream = m_Stream
    
    Exit Function
    
Errores:
        
End Function

Public Function PopImg()

End Function

Function PushImageToDb(PathImage As String, RecSet As ADODB.Recordset, Campo As Variant) As Boolean
    
    Dim LibreArc As Integer, Buffer As Variant
    
    Dim FS, f, s, counter As Long
    
    PushImageToDb = False
    
    'VERIFICA SI EXISTE PARA BORRARLO ANTES DE EMPEZAR
    
    sTemp = PathImage
    
    Set FS = CreateObject("Scripting.FileSystemObject")
    
    If Not FS.FileExists(PathImage) Then
        Exit Function
    End If
    
    LibreArc = FreeFile()
    FilTam = FileLen(PathImage)
    
    Open PathImage For Binary Access Read As #LibreArc
    
    Buffer = Input(LOF(LibreArc), LibreArc)
    RecSet.Fields(Campo).AppendChunk (Buffer)
    'RecSet.UpdateBatch
    
    Close LibreArc
    
    PushImageToDb = True
    
    'VERIFICA SI EXISTE PARA BORRARLO AL TERMINAR
    
End Function

'Funciones auxiliares para trabajar con Collections.

Public Function Collection_AddKey(pCollection As Collection, pValor, pKey As String) As Boolean
    
    On Error GoTo Err
    
    pCollection.Add pValor, pKey
    
    Collection_AddKey = True
    
    Exit Function
    
Err:
    
    Debug.Print Err.Description
    
    Collection_AddKey = False
    
End Function

'Public Function Collection_IgualA(pCollection As Collection, pValor) As Boolean
'
'    On Error GoTo Err
'
'    Dim i As Long
'
'    For i = 1 To pCollection.Count
'        If pCollection.Item(i) = pValor Then Collection_IgualA = True: Exit Function
'    Next i
'
'    Exit Function
'
'Err:
'
'    Debug.Print Err.Description
'
'    Collection_IgualA = False
'
'End Function

Public Function Collection_EncontrarValor(pCollection As Collection, pValor, Optional pIndiceStart As Long = 1) As Long
    
    On Error GoTo Err
    
    Dim i As Long
    
    For i = pIndiceStart To pCollection.Count
        If pCollection.Item(i) = pValor Then Collection_EncontrarValor = i: Exit Function
    Next i
    
    Collection_EncontrarValor = -1
    
    Exit Function
    
Err:
    
    Debug.Print Err.Description
    
    Collection_EncontrarValor = -1
    
End Function

Public Function Collection_ExisteKey(pCollection As Collection, pKey As String, Optional pItemsAreObjects As Boolean = False) As Boolean

    On Error GoTo Err
    
    Dim tmpValor As Variant
    Dim tmpValorObj As Object
    
    If Not pItemsAreObjects Then
        tmpValor = pCollection.Item(pKey)
    Else
        Set tmpValorObj = pCollection.Item(pKey)
    End If
     
    Collection_ExisteKey = True
     
    Exit Function
        
Err:
    
    'Debug.Print Err.Description
    
    Collection_ExisteKey = False
    
End Function

Public Function Collection_ExisteIndex(pCollection As Collection, pIndex As Long, Optional pItemsAreObjects As Boolean = False) As Boolean

    On Error GoTo Err
    
    Dim tmpValor As Variant
    Dim tmpValorObj As Object
    
    If Not pItemsAreObjects Then
        tmpValor = pCollection.Item(pIndex)
    Else
        Set tmpValorObj = pCollection.Item(pIndex)
    End If
     
    Collection_ExisteIndex = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_ExisteIndex = False
    
End Function

Public Function Collection_RemoveKey(pCollection As Collection, pKey As String) As Boolean

    On Error GoTo Err
    
    pCollection.Remove (pKey)
        
    Collection_RemoveKey = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_RemoveKey = False
    
End Function

Public Function Collection_RemoveIndex(pCollection As Collection, pIndex As Long) As Boolean

    On Error GoTo Err
    
    pCollection.Remove (pKey)
        
    Collection_RemoveIndex = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_RemoveIndex = False
    
End Function

Public Sub ShowPicturePreviewOnForm(ActiveForm As Object, PicObj)
    
    On Error Resume Next
    
    Dim TmpContainer As Object, TmpPicture As Object
    
    Set TmpContainer = ActiveForm.TmpPreviewPicContainer
    Set TmpPicture = ActiveForm.TmpPreviewPic
    
    Set ActiveForm.TmpPreviewPicContainer = ActiveForm.Controls.Add("VB.PictureBox", "TmpPreviewPicContainer", ActiveForm)
    Set ActiveForm.TmpPreviewPic = ActiveForm.Controls.Add("VB.Image", "TmpPreviewPic", ActiveForm)

    TmpPicture.Stretch = False
    
    TmpContainer.ScaleMode = vbTwips
    
    TmpPicture.Picture = PicObj
    
    Set TmpPicture.Container = TmpContainer
    
    TmpPicture.Left = 0
    TmpPicture.Top = 0
    TmpPicture.Visible = True
    
    TmpPicture.Left = 0
    TmpPicture.Top = 0
    
    TmpContainer.Width = TmpPicture.Width
    TmpContainer.Height = TmpPicture.Height
    
    If TmpContainer.Width > ActiveForm.Width Or _
    TmpContainer.Height > ActiveForm.Height Then
        
        If TmpContainer.Width > ActiveForm.Width Then
            TmpContainer.Width = ActiveForm.Width
        End If
        
        If TmpContainer.Height > ActiveForm.Height Then
            TmpContainer.Height = ActiveForm.Height
        End If
        
        TmpPicture.Width = TmpContainer.Width
        TmpPicture.Height = TmpContainer.Height
        
        TmpContainer.Left = ActiveForm.Width / 2 - TmpContainer.Width / 2
        TmpContainer.Top = ActiveForm.Height / 2 - TmpContainer.Height / 2
        
        TmpPicture.Stretch = True
        
    Else
        
        TmpContainer.Left = ActiveForm.Width / 2 - TmpContainer.Width / 2
        TmpContainer.Top = ActiveForm.Height / 2 - TmpContainer.Height / 2
        
    End If
    
    TmpContainer.Visible = True
    
    TmpContainer.ZOrder 0

End Sub

Public Function FechaBD(ByVal Expression, Optional pTipo As FechaBDPrecision = FBD_Fecha, _
Optional pGrabarRecordset As Boolean = False) As String
    
    If Not pGrabarRecordset Then
    
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYYMMDD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYYMMDD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                Dim dDate As Date
                Dim dMilli As Double
                Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYYMMDD HH:mm:ss") '& Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Right(FechaBD(Expression, FBD_FULL), 12)
                
        End Select
    
    Else
        
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYY-MM-DD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYY-MM-DD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                'Dim dDate As Date
                'Dim dMilli As Double
                'Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYY-MM-DD HH:mm:ss") '& Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Format(Expression, "HH:mm:ss") 'Right(FechaBD(Expression, FBD_Full), 12)
                ' El Formato de Hora Full (Con MS) No funciona de manera confiable en recordset.
                ' Si a�n as� se desea utilizar este formato, volver a llamar a esta funci�n sin pGrabarRecordset
                
        End Select
        
    End If
    
End Function

Public Function GetEnvironmentVariable(Expression) As String
    On Error Resume Next
    GetEnvironmentVariable = Environ$(Expression)
End Function

Public Function KillSecure(ByVal pFile As String) As Boolean
    On Error GoTo KS
    Kill pFile: KillSecure = True
    Exit Function
KS:
    'Debug.Print Err.Description
    Err.Clear 'Ignorar.
End Function

Public Function KillShot(ByVal pFile As String) As Boolean
    KillShot = KillSecure(pFile)
End Function

Public Function PathExists(pPath As String) As Boolean
    'On Error Resume Next
    ' For Files it works normally, but for Directories,
    ' pPath must end in "\" to be able to find them.
    If Dir(pPath, vbArchive Or vbDirectory Or vbHidden) <> "" Then PathExists = True
End Function

Public Function GetDirectoryRoot(pPath As String) As String

    Dim Pos As Long
    
    Pos = InStr(1, pPath, "\")
    
    If Pos <> 0 Then
        GetDirectoryRoot = Left(pPath, Pos - 1)
    Else
        GetDirectoryRoot = vbNullString
    End If
    
End Function

Public Function GetDirParent(pPath As String) As String

    Dim Pos As Long
    
    Pos = InStrRev(pPath, "\")
    
    If Pos <> 0 Then
        GetDirParent = Left(pPath, Pos - 1)
    Else
        GetDirParent = vbNullString
    End If

End Function

Public Function CopyPath(Source As String, Destination As String) As Boolean
    
    On Error GoTo ErrCP
    
    Dim FSO As New FileSystemObject
    
    FSO.CopyFile Source, Destination
    
    CopyPath = True
    
    Exit Function
    
ErrCP:

    CopyPath = False
    
    'Debug.Print Err.Description
    
    Err.Clear
    
End Function

Public Function FindPath(ByVal FileName As String, FindFileInUpperLevels As FindFileConstants, _
Optional ByVal BaseFilePath As String = "$(AppPath)") As String

    Dim CurrentDir As String
    Dim CurrentFilePath As String
    Dim Exists As Boolean
    Dim NetworkPath As Boolean
    
    If BaseFilePath = "$(AppPath)" Then BaseFilePath = App.Path
    
    FileName = Replace(FileName, "/", "\"): BaseFilePath = Replace(BaseFilePath, "/", "\")
    
    BaseFilePath = BaseFilePath & IIf(Right(BaseFilePath, 1) = "\", vbNullString, "\")

    If (FindFileInUpperLevels = FindFileConstants.NO_SEARCH_MUST_FIND_EXACT_MATCH) Then
        
        ' Anular riesgo de quedar con una ruta mal formada por exceso o escasez de literales, tomando en cuenta que adem�s puede ser una ruta de red.
        ' Por lo tanto a esta funcion le podemos pasar el BaseFilePath a�n con exceso de "\\" sin preocuparse por ello.
        
        ' Por ejemplo si se llama a la funcion y se le suministra el siguiente BaseFilePath:
        ' \\10.10.1.100\Public\TMP\\OtraRuta\\CarpetaFinal
        ' se arreglar� de la siguiente forma \\10.10.1.100\Public\TMP\OtraRuta\CarpetaFinal\
        ' y se devolver� BaseFilePath + FileName, un path siempre v�lido.
        
        ' Nota: Est� funci�n asegura la construcci�n v�lida de la ruta m�s no garantiza la existencia de la misma.
        
        FindPath = BaseFilePath & IIf(FileName Like "*.*", FileName, FileName & "\")
        
        While FindPath Like "*\\*" And Not NetworkPath
            If Left(FindPath, 2) = "\\" And (Right(Replace(StrReverse(FindPath), "\\", "\", , 1), 2) <> "\\") Then
                NetworkPath = True
            Else
                FindPath = StrReverse(Replace(StrReverse(FindPath), "\\", "\", , 1))
            End If
        Wend
                
        ' Ready.
                
    ElseIf (FindFileInUpperLevels = FindFileConstants.SEARCH_ALL_UPPER_LEVELS) Then
            
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        While (CurrentDir <> GetDirectoryRoot(BaseFilePath))
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
            
            If Exists Then FindPath = CurrentFilePath: Exit Function
        
            CurrentDir = GetDirParent(CurrentDir)
        
        Wend
            
        CurrentFilePath = CurrentDir + "\" + FileName
        
        Exists = PathExists(CurrentFilePath)
        
        If Not Exists Then
        
            CurrentFilePath = CurrentDir + "\" + FileName + "\"
            
            Exists = PathExists(CurrentFilePath)
                
        End If
        
        If Exists Then
            FindPath = CurrentFilePath: Exit Function
        Else
            CurrentFilePath = CurrentDir + "\" + FileName
        End If
        
        FindPath = BaseFilePath + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    ElseIf (FindFileInUpperLevels > 0) Then
        
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        On Error GoTo PathEOF
        
        For i = 1 To FindFileInUpperLevels
        
            If CurrentDir = vbNullString Then Exit For
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
                    
            If Exists Then
                FindPath = CurrentFilePath: Exit Function
            Else
                CurrentFilePath = CurrentDir + "\" + FileName
            End If
            
            CurrentDir = GetDirParent(CurrentDir)
        
        Next i
        
PathEOF:
        
        FindPath = CurrentDir + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    End If

End Function

Public Function CreateFullDirectoryPath(ByVal pDirectoryPath As String) As Boolean

    On Error GoTo Error
    
    Dim TmpDirectoryPath As String, TmpLVL As Integer, StartLVL As Integer
    
    pDirectoryPath = GetDirParent(pDirectoryPath)
    
    Dim PathArray As Variant
    
    PathArray = Split(Replace(pDirectoryPath, "\\", "\"), "\")
    
    ' Determine Existing Path Level
    
    TmpDirectoryPath = pDirectoryPath
    
    While Not PathExists(FindPath(vbNullString, NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath))
        TmpLVL = TmpLVL + 1
        TmpDirectoryPath = FindPath(vbNullString, SEARCH_2_UPPER_LEVELS, TmpDirectoryPath)
    Wend
    
    StartLVL = (UBound(PathArray) + 1) - TmpLVL
    
    For i = StartLVL To (UBound(PathArray))
        TmpDirectoryPath = FindPath(CStr(PathArray(i)), NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath)
        MkDir TmpDirectoryPath
    Next i
    
    CreateFullDirectoryPath = True
    
    Exit Function
    
Error:
    
    CreateFullDirectoryPath = False
    
End Function

Public Function AsEnumerable(ArrayList As Variant) As Collection
    
    Set AsEnumerable = New Collection
    
    On Error Resume Next
    
    For i = LBound(ArrayList) To UBound(ArrayList)
        AsEnumerable.Add ArrayList(i), CStr(i)
    Next i
    
End Function

Public Function BuscarReglaNegocioStr(pConexion As Object, pCampo, Optional pDefault As String = "") As String
    
    Dim mRs As New ADODB.Recordset
    Dim mSql As String
    
    On Error GoTo Errores
    
    mSql = "SELECT * FROM MA_REGLASDENEGOCIO WHERE Campo = '" & pCampo & "'"
    mRs.Open mSql, pConexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarReglaNegocioStr = CStr(mRs!Valor)
    Else
        BuscarReglaNegocioStr = pDefault
    End If
    
    Exit Function
    
Errores:
    
    Err.Clear
    
End Function
