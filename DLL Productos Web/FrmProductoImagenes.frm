VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "ComDlg32.ocx"
Begin VB.Form FrmProductoImagenes 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7770
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15270
   ControlBox      =   0   'False
   Icon            =   "FrmProductoImagenes.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7770
   ScaleWidth      =   15270
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton CmdGrabar 
      Caption         =   "Grabar"
      Height          =   1035
      Left            =   960
      Picture         =   "FrmProductoImagenes.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   5400
      Width           =   1140
   End
   Begin VB.CommandButton CmdCancelar 
      Caption         =   "Cancelar"
      Height          =   1035
      Left            =   2640
      Picture         =   "FrmProductoImagenes.frx":800C
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   5400
      Width           =   1140
   End
   Begin VB.PictureBox PicLoad 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   12720
      ScaleHeight     =   495
      ScaleWidth      =   2415
      TabIndex        =   18
      Top             =   480
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.Frame FrameTitulo 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   15360
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14640
         Picture         =   "FrmProductoImagenes.frx":9D8E
         Top             =   -30
         Width           =   480
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12555
         TabIndex        =   2
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Productos Web - Im�genes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   75
         Width           =   5295
      End
   End
   Begin MSComDlg.CommonDialog ImagePicker 
      Left            =   12120
      Top             =   480
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      DialogTitle     =   "Buscar Imagenes"
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "IMAGENES ADICIONALES - VISTA WEB"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   240
      Left            =   8040
      TabIndex        =   22
      Top             =   720
      Width           =   3315
   End
   Begin VB.Line LineaV 
      BorderWidth     =   2
      X1              =   4920
      X2              =   4920
      Y1              =   1200
      Y2              =   7400
   End
   Begin VB.Line LineaH 
      BorderWidth     =   2
      X1              =   240
      X2              =   15000
      Y1              =   4320
      Y2              =   4320
   End
   Begin VB.Label lblImagenPrincipal 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "IMAGEN PRINCIPAL (FICHA DEL PRODUCTO)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   240
      Left            =   480
      TabIndex        =   21
      Top             =   720
      Width           =   3780
   End
   Begin VB.Image Imagen 
      Appearance      =   0  'Flat
      Height          =   2715
      Index           =   4
      Left            =   10440
      Stretch         =   -1  'True
      Top             =   4680
      Width           =   4275
   End
   Begin VB.Image Imagen 
      Appearance      =   0  'Flat
      Height          =   2715
      Index           =   3
      Left            =   5520
      Stretch         =   -1  'True
      Top             =   4680
      Width           =   4275
   End
   Begin VB.Image Imagen 
      Appearance      =   0  'Flat
      Height          =   2715
      Index           =   2
      Left            =   10440
      Stretch         =   -1  'True
      Top             =   1200
      Width           =   4275
   End
   Begin VB.Image Imagen 
      Appearance      =   0  'Flat
      Height          =   2715
      Index           =   1
      Left            =   5520
      Stretch         =   -1  'True
      Top             =   1200
      Width           =   4275
   End
   Begin VB.Image Imagen 
      Appearance      =   0  'Flat
      Height          =   2715
      Index           =   0
      Left            =   240
      Stretch         =   -1  'True
      Top             =   1200
      Width           =   4275
   End
   Begin VB.Label lblClickDerecho 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Click Derecho para eliminar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   240
      Index           =   4
      Left            =   11415
      TabIndex        =   17
      Top             =   6285
      Width           =   2370
   End
   Begin VB.Label lblDobleClick 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Doble Click para cambiar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   240
      Index           =   4
      Left            =   11520
      TabIndex        =   16
      Top             =   5925
      Width           =   2130
   End
   Begin VB.Label lblImagen 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Imagen"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   375
      Index           =   4
      Left            =   12015
      TabIndex        =   15
      Top             =   5445
      Width           =   1080
   End
   Begin VB.Label lblClickDerecho 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Click Derecho para eliminar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   240
      Index           =   3
      Left            =   6495
      TabIndex        =   14
      Top             =   6285
      Width           =   2370
   End
   Begin VB.Label lblDobleClick 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Doble Click para cambiar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   240
      Index           =   3
      Left            =   6600
      TabIndex        =   13
      Top             =   5925
      Width           =   2130
   End
   Begin VB.Label lblImagen 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Imagen"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   375
      Index           =   3
      Left            =   7095
      TabIndex        =   12
      Top             =   5445
      Width           =   1080
   End
   Begin VB.Label lblClickDerecho 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Click Derecho para eliminar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   240
      Index           =   2
      Left            =   11415
      TabIndex        =   11
      Top             =   2805
      Width           =   2370
   End
   Begin VB.Label lblDobleClick 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Doble Click para cambiar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   240
      Index           =   2
      Left            =   11520
      TabIndex        =   10
      Top             =   2445
      Width           =   2130
   End
   Begin VB.Label lblImagen 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Imagen"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   375
      Index           =   2
      Left            =   12015
      TabIndex        =   9
      Top             =   1965
      Width           =   1080
   End
   Begin VB.Label lblClickDerecho 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Click Derecho para eliminar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   240
      Index           =   1
      Left            =   6495
      TabIndex        =   8
      Top             =   2805
      Width           =   2370
   End
   Begin VB.Label lblDobleClick 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Doble Click para cambiar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   240
      Index           =   1
      Left            =   6600
      TabIndex        =   7
      Top             =   2445
      Width           =   2130
   End
   Begin VB.Label lblImagen 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Imagen"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   375
      Index           =   1
      Left            =   7095
      TabIndex        =   6
      Top             =   1965
      Width           =   1080
   End
   Begin VB.Label lblClickDerecho 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Click Derecho para eliminar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   240
      Index           =   0
      Left            =   1215
      TabIndex        =   5
      Top             =   2805
      Width           =   2370
   End
   Begin VB.Label lblDobleClick 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Doble Click para cambiar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   240
      Index           =   0
      Left            =   1320
      TabIndex        =   4
      Top             =   2445
      Width           =   2130
   End
   Begin VB.Label lblImagen 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Imagen"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   375
      Index           =   0
      Left            =   1815
      TabIndex        =   3
      Top             =   1965
      Width           =   1080
   End
End
Attribute VB_Name = "FrmProductoImagenes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public fCls As clsProductosWeb

Public WithEvents TmpPreviewPic As Image
Attribute TmpPreviewPic.VB_VarHelpID = -1
Public TmpPreviewPicContainer As PictureBox

Private Const TmpImgFileBaseName = "StellarProductPicTmp$(Index).$(Ext)"
Private TmpImgFileName As String
Private TmpFileNameCollection As Dictionary
Private TmpSelectedIndex As Integer

Private TmpFolder As String

Private FormaCargada As Boolean
Private CodigoProducto As String

Property Get Producto() As String
    Producto = CodigoProducto
End Property

Property Let Producto(mValor As String)
    CodigoProducto = mValor
End Property

Private Sub FrameTitulo_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbLeftButton Then MoverVentana Me.hWnd
End Sub

Private Sub lbl_Organizacion_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    FrameTitulo_MouseMove Button, Shift, x, y
End Sub

Private Sub lbl_website_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    FrameTitulo_MouseMove Button, Shift, x, y
End Sub

Private Function GetFileProp(Index As Integer, Property As String) As Variant
    
    On Error GoTo ErrFileProperties
    
    Dim FSO As New FileSystemObject, TmpFile As File, TmpVar As Variant
    
    Set TmpFile = FSO.GetFile(TmpFileNameCollection(CStr(Index)))
        
    Select Case UCase(Property)
        Case UCase("Type")
            GetFileProp = TmpFile.Type
        Case UCase("Extension")
            TmpVar = Strings.Split(TmpFile.Path, ".")
            GetFileProp = "." & TmpVar(UBound(TmpVar))
        Case UCase("Description")
            GetFileProp = Null
        Case UCase("Size")
            GetFileProp = TmpFile.Size
    End Select
    
    Exit Function
    
ErrFileProperties:
    
    GetContentType = Null
    
End Function

Private Sub Grabar()
    
    On Error GoTo ErrSave
    
    Dim mRs As Recordset, InitTrans As Boolean, ObjectTemp As Object, DBNull As Variant
    
    fCls.ClsCn.Conexion.BeginTrans
    
    InitTrans = True
    
    Set mRs = New Recordset
    
    mRs.Open "SELECT * FROM MA_PRODUCTOS WHERE c_Codigo = '" & CodigoProducto & "'", _
    fCls.ClsCn.Conexion, adOpenKeyset, adLockPessimistic, adCmdText
    
    If Not mRs.EOF Then
        
        If TmpFileNameCollection.Exists(CStr(0)) And Imagen(0).Picture.Handle <> 0 Then
            Call PushImageToDb(TmpFileNameCollection(CStr(0)), mRs, "c_FileImagen")
        ElseIf Imagen(0).Picture.Handle = 0 And Not IsNull(mRs!C_FILEIMAGEN) And Not IsEmpty(mRs!C_FILEIMAGEN) Then
            mRs!C_FILEIMAGEN = Null
        Else
            mRs.Close
            Set mRs = Nothing
        End If
        
        If Not mRs Is Nothing Then
            
            mRs!c_UsuarioUpd = LcCodUsuario
            mRs!Update_Date = FechaBD(Now, FBD_FULL, True) 'Date
            
            mRs.Update
            
            fCls.AgregarPendienteProd fCls.ClsCn.Conexion, mRs
            
            mRs.Close
            
        End If
        
    End If
    
    For Each Ctl In Imagen
        
        If Ctl.Index = 0 Then GoTo Continue
        
        Set mRs = New Recordset
        
        mRs.Open "SELECT * FROM MA_PRODUCTOS_IMAGENES WHERE CodigoProducto = '" & CodigoProducto & "' AND Orden = " & Ctl.Index, fCls.ClsCn.Conexion, adOpenKeyset, adLockPessimistic, adCmdText
        
        If mRs.EOF Then
            mRs.AddNew
            mRs!CodigoProducto = CodigoProducto
            mRs!Orden = Ctl.Index
        Else
            mRs!Fecha = fCls.ClsCn.Conexion.Execute("Select GetDate() AS Fecha")!Fecha
            mRs!cs_Numero_Transferencia = vbNullString
        End If
            
        If TmpFileNameCollection.Exists(CStr(Ctl.Index)) And Ctl.Picture.Handle <> 0 Then
        
            Set ObjectTemp = PopFileStream(TmpFileNameCollection(CStr(Ctl.Index)))
            
            If Not ObjectTemp Is Nothing Then
                mRs("Imagen").AppendChunk (ObjectTemp.Read)
            End If
            
            mRs!MIMEType = GetFileProp(Ctl.Index, "Type")
            mRs!Extension = GetFileProp(Ctl.Index, "Extension")
            mRs!Descripcion = GetFileProp(Ctl.Index, "Description")
            mRs!Bytes = GetFileProp(Ctl.Index, "Size")
        
        ElseIf Ctl.Picture.Handle = 0 And Not IsNull(mRs!Imagen) And Not IsEmpty(mRs!Imagen) Then
            
            mRs!Imagen = Null
            mRs!MIMEType = Null
            mRs!Extension = Null
            mRs!Descripcion = Null
            mRs!Bytes = Null
            
        Else
        
            ' NO CAMBIO
            mRs.CancelUpdate
            mRs.Close
            GoTo Continue
            
        End If
        
        mRs.Update
        
Continue:
        
    Next
    
    fCls.ClsCn.Conexion.CommitTrans
    
    Exit_Click
    
    Exit Sub
    
ErrSave:
    
    Resume ' Debug
    
    If InitTrans Then fCls.ClsCn.Conexion.RollbackTrans
    
    fCls.Mensaje "Ha ocurrido un error al guardar los datos. Por favor reporte lo siguiente: " & Err.Description
    
End Sub

Private Sub CmdGrabar_Click()
    Call Grabar
End Sub

Private Sub Form_Unload(Cancel As Integer)
    For Each Ctl In Imagen
        TmpImgFileName = Replace(Replace(TmpImgFileBaseName, "$(Index)", Ctl.Index), "$(Ext)", "jpg")
        KillSecure TmpFolder & TmpImgFileName
    Next
End Sub

Private Sub TmpPreviewPic_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    
    On Error Resume Next
    
    If Button = vbRightButton Or Button = vbMiddleButton Then
        
        If Not TmpFileNameCollection.Exists(CStr(TmpSelectedIndex)) Then
            ShowPic TmpSelectedIndex
        Else
            ShellExecute 0, vbNullString, "" & TmpFileNameCollection(CStr(TmpSelectedIndex)) & "", vbNullString, vbNullString, 1
        End If
        
        TmpPreviewPic_MouseUp vbLeftButton, Shift, x, y
        
    ElseIf Button = vbLeftButton Then
        Me.Controls.Remove TmpPreviewPic
        Me.Controls.Remove TmpPreviewPicContainer
    End If
    
End Sub

Private Sub ShowPic(Index As Integer)

    On Error Resume Next
    
    TmpImgFileName = Replace(Replace(TmpImgFileBaseName, "$(Index)", Index), "$(Ext)", "jpg")
    
    KillSecure TmpFolder & TmpImgFileName
    
    Dim mSql As String, mRs As Recordset
    
    Set mRs = New Recordset
    
    Select Case Index
        
        Case 0
            
            mSql = "SELECT c_FileImagen FROM MA_PRODUCTOS WHERE c_Codigo = '" & CodigoProducto & "'"
            
            mRs.Open mSql, fCls.ClsCn.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
            
            If Not mRs.EOF Then
                
                If Not IsNull(mRs!C_FILEIMAGEN) Then
                    
                    Call PopImageOfDb(Imagen(Index), mRs, "c_FileImagen", TmpFolder & TmpImgFileName)
                    
                    Dim TmpFS As New Scripting.FileSystemObject
                    
                    If TmpFS.FileExists(TmpFolder & TmpImgFileName) Then
                        ShellExecute 0, vbNullString, "" & TmpFS.GetAbsolutePathName(TmpFolder & TmpImgFileName) & "", vbNullString, vbNullString, 1
                    End If
                    
                End If
                
            End If
            
        Case Else
            
            mSql = "SELECT Imagen FROM MA_PRODUCTOS_IMAGENES WHERE CodigoProducto = '" & CodigoProducto & "' AND Orden = " & Index
            
            mRs.Open mSql, fCls.ClsCn.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
            
            If Not mRs.EOF Then
                
                If Not IsNull(mRs!Imagen) Then
                    
                    Call PopStreamOfDb(Imagen(Index), mRs, "Imagen", TmpFolder & TmpImgFileName)
                    
                    If TmpFS.FileExists(TmpFolder & TmpImgFileName) Then
                        ShellExecute 0, vbNullString, "" & TmpFS.GetAbsolutePathName(TmpFolder & TmpImgFileName) & "", vbNullString, vbNullString, 1
                    End If
                    
                End If
                
            End If
            
    End Select
    
End Sub

Private Sub CmdCancelar_Click()
    Exit_Click
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    If Not FormaCargada Then
        Unload Me
    End If
End Sub

Private Sub Form_Load()
    
    For Each Ctl In Imagen
        TmpImgFileName = Replace(Replace(TmpImgFileBaseName, "$(Index)", Ctl.Index), "$(Ext)", "jpg")
        KillSecure TmpFolder & TmpImgFileName
    Next
    
    FormaCargada = CargarImagenes
    Set TmpFileNameCollection = New Dictionary
    
End Sub

Private Function CargarImagenes() As Boolean
    
    On Error GoTo ErrLoad
    
    TmpFolder = GetEnvironmentVariable("ProgramData")
    
    If TmpFolder <> vbNullString Then
        TmpFolder = FindPath(SoftwarePath, NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpFolder)
        CreateFullDirectoryPath TmpFolder
    Else
        TmpFolder = FindPath(vbNullString, NO_SEARCH_MUST_FIND_EXACT_MATCH)
    End If
    
    Dim mRs As Recordset
    
    Set mRs = fCls.ClsCn.Conexion.Execute("SELECT c_FileImagen FROM MA_PRODUCTOS WHERE c_Codigo = '" & CodigoProducto & "'")
    
    If Not mRs.EOF Then
        If Not IsNull(mRs!C_FILEIMAGEN) Then
            TmpImgFileName = Replace(Replace(TmpImgFileBaseName, "$(Index)", 0), "$(Ext)", "jpg")
            Call PopImageOfDb(Imagen(0), mRs, "c_FileImagen", TmpFolder & TmpImgFileName)
        Else
            Imagen(0).Picture = LoadPicture
        End If
    End If
    
    Set mRs = fCls.ClsCn.Conexion.Execute("SELECT * FROM MA_PRODUCTOS_IMAGENES WHERE CodigoProducto = '" & CodigoProducto & "' AND Orden BETWEEN 1 AND 4")
    
    While Not mRs.EOF
        If Not IsNull(mRs!Imagen) Then
            TmpImgFileName = Replace(Replace(TmpImgFileBaseName, "$(Index)", mRs!Orden), "$(Ext)", "jpg")
            If UCase(mRs!Extension) Like UCase("*.png") Then
                Call PopStreamOfDb(Imagen(mRs!Orden), mRs, "Imagen", TmpFolder & TmpImgFileName)
                LoadFileToControl PicLoad, TmpFolder & TmpImgFileName
                Set Imagen(mRs!Orden).Picture = PicLoad.Picture
            Else
                Call PopStreamOfDb(Imagen(mRs!Orden), mRs, "Imagen", TmpFolder & TmpImgFileName)
            End If
        End If
        mRs.MoveNext
    Wend
    
    'DBNull = fCls.ClsCn.Conexion.Execute("SELECT NULL AS DBNull")!DBNull
    
    CargarImagenes = True
    
    Exit Function
    
ErrLoad:
    
    fCls.Mensaje "Ha ocurrido un error al cargar los datos. Por favor reporte lo siguiente: " & Err.Description
    
End Function

Private Sub Imagen_DblClick(Index As Integer)
    
    On Error GoTo Errores
    
    MousePointer = vbHourglass
    Me.Enabled = False
    
    ImagePicker.CancelError = True
    
    TmpSelectedIndex = Index
    TmpPreviewPic_MouseUp vbLeftButton, 0, 0, 0
        
    Select Case Index
        Case 0
            ImagePicker.Filter = "Im�genes (*.jpg)|*.jpg"
        Case Else
            ImagePicker.Filter = "Im�genes (*.jpg;*.png;*.gif;*.bmp;*.wmf;*.ico;*.cur)|*.jpg;*.png;*.gif;*.bmp;*.wmf;*.ico;*.cur"
    End Select
    
    If Not TmpFileNameCollection.Exists(CStr(Index)) Then
        ImagePicker.FileName = vbNullString
    Else
        ImagePicker.FileName = TmpFileNameCollection(CStr(Index))
    End If
    
    ImagePicker.ShowOpen
    
    If ImagePicker.FileName <> vbNullString Then
        
        Select Case Index
            Case 0 ' Imagen Principal del Producto / ThumbNail
                Set Imagen(0).Picture = LoadPicture(ImagePicker.FileName)
            Case Else ' Im�genes Alternativas
                
                If UCase(ImagePicker.FileName) Like UCase("*.png") Then
                    LoadFileToControl PicLoad, ImagePicker.FileName
                    
                    Set Imagen(Index).Picture = PicLoad.Picture
                Else
                    Set Imagen(Index).Picture = LoadPicture(ImagePicker.FileName)
                End If
                
        End Select
        
        If Not TmpFileNameCollection.Exists(CStr(Index)) Then
            TmpFileNameCollection.Add CStr(Index), ImagePicker.FileName
        Else
            TmpFileNameCollection.Item(CStr(Index)) = ImagePicker.FileName
        End If
        
    End If
    
Finally:
    
    MousePointer = vbDefault
    
    PreventExtraClicks ' Rehabilitar Form con Timer
    
    Exit Sub
    
Errores:
    
    If Err.Number <> 32755 Then
        fCls.Mensaje "Ha ocurrido un error al seleccionar el archivo. Por favor reporte lo siguiente: " & Err.Description
    Else
        ' No pasa nada... este error es un artificio para cuando el usuario cancelo la selecci�n.
    End If
    
    GoTo Finally
    
End Sub

Private Sub PreventExtraClicks()
    
    If Not GlobalEventTimer Is Nothing Then Set GlobalEventTimer = Nothing
    
    Set GlobalEventTimer = New SelfTimer
    
    GlobalEventTimer.IsGlobalEventHandler = True
        
    ' Inicializar Delegate con los par�metros.
    
    GlobalEventTimer.EventDelegate = New clsDelegate
    
    With GlobalEventTimer.EventDelegate
        .Initialize
        
        .Name = "AllowCtlEvents"
        
        If Not IsMissing(pControl) Then .SetParam "pControl", "Object", Me
    End With
    
    ' Esperar...
    
    GlobalEventTimer.Interval = 500
    GlobalEventTimer.Enabled = True
    
End Sub

Private Sub Imagen_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbRightButton Then
        If fCls.Mensaje("Si desea eliminar la imagen, presione Aceptar.", True) Then
            Imagen(Index).Picture = LoadPicture()
        End If
    ElseIf Button = vbLeftButton Then
        If Imagen(Index).Picture.Handle <> 0 Then
            ShowPicturePreviewOnForm Me, Imagen(Index).Picture
        Else
            Imagen_DblClick Index
        End If
    ElseIf Button = vbMiddleButton Then
        TmpSelectedIndex = Index
        TmpPreviewPic_MouseUp vbRightButton, 0, 0, 0
    End If
End Sub
