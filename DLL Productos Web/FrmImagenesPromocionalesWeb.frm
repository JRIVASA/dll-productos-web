VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "ComDlg32.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RichTx32.ocx"
Begin VB.Form FrmImagenesPromocionalesWeb 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9300
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15270
   ControlBox      =   0   'False
   Icon            =   "FrmImagenesPromocionalesWeb.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9300
   ScaleWidth      =   15270
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame FrameTemas 
      BackColor       =   &H00BA9D8B&
      BorderStyle     =   0  'None
      Caption         =   "Frame3"
      Height          =   735
      Left            =   12120
      TabIndex        =   23
      Top             =   1680
      Width           =   1575
      Begin VB.Label LblTemas 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Tema Web"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   120
         TabIndex        =   24
         Top             =   240
         Width           =   1335
      End
   End
   Begin RichTextLib.RichTextBox txtLink 
      Height          =   855
      Left            =   4920
      TabIndex        =   22
      Top             =   3000
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   1508
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   2
      Appearance      =   0
      TextRTF         =   $"FrmImagenesPromocionalesWeb.frx":628A
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.TextBox txtDescri 
      Height          =   855
      Left            =   4920
      MultiLine       =   -1  'True
      TabIndex        =   20
      Top             =   1560
      Width           =   5655
   End
   Begin VB.TextBox txtLinkNormal 
      Height          =   855
      Left            =   4920
      MultiLine       =   -1  'True
      TabIndex        =   18
      Top             =   3000
      Width           =   5655
   End
   Begin VB.Frame frmItems 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   4095
      Left            =   240
      TabIndex        =   11
      Top             =   4800
      Width           =   14775
      Begin VB.TextBox txtEdit 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2400
         TabIndex        =   16
         Top             =   2280
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Frame FrmAgregar 
         BackColor       =   &H0000C000&
         BorderStyle     =   0  'None
         Caption         =   "Falta eliminar plato"
         Height          =   495
         Left            =   12120
         TabIndex        =   14
         Top             =   840
         Width           =   2055
         Begin VB.Label Agregar 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Agregar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   375
            Left            =   360
            TabIndex        =   15
            Top             =   100
            Width           =   1455
         End
      End
      Begin VB.Frame FrmEliminar 
         BackColor       =   &H00808080&
         BorderStyle     =   0  'None
         Caption         =   "Frame3"
         Height          =   495
         Left            =   12120
         TabIndex        =   12
         Top             =   1800
         Width           =   2055
         Begin VB.Label Eliminar 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Eliminar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   360
            TabIndex        =   13
            Top             =   120
            Width           =   1335
         End
      End
      Begin MSFlexGridLib.MSFlexGrid Grid 
         Height          =   3615
         Left            =   240
         TabIndex        =   17
         Top             =   240
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   6376
         _Version        =   393216
         FixedCols       =   0
         RowHeightMin    =   360
         ForeColor       =   5790296
         BackColorFixed  =   11426560
         ForeColorFixed  =   16448250
         BackColorSel    =   -2147483644
         ForeColorSel    =   11426560
         BackColorBkg    =   -2147483643
         GridColorFixed  =   -2147483643
         FillStyle       =   1
         GridLines       =   0
         GridLinesFixed  =   0
         SelectionMode   =   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Image CmdTeclado 
         Height          =   600
         Left            =   13800
         MouseIcon       =   "FrmImagenesPromocionalesWeb.frx":6309
         MousePointer    =   99  'Custom
         Picture         =   "FrmImagenesPromocionalesWeb.frx":6613
         Stretch         =   -1  'True
         Top             =   -360
         Visible         =   0   'False
         Width           =   600
      End
   End
   Begin VB.PictureBox PicLoad 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   12720
      ScaleHeight     =   495
      ScaleWidth      =   2415
      TabIndex        =   9
      Top             =   480
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.CommandButton CmdGrabar 
      Caption         =   "Grabar"
      Height          =   1035
      Left            =   12240
      Picture         =   "FrmImagenesPromocionalesWeb.frx":66BF
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   2880
      Width           =   1140
   End
   Begin VB.CommandButton CmdCancelar 
      Caption         =   "Cancelar"
      Height          =   1035
      Left            =   13680
      Picture         =   "FrmImagenesPromocionalesWeb.frx":8441
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   2880
      Width           =   1140
   End
   Begin VB.Frame FrameTitulo 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   15360
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14640
         Picture         =   "FrmImagenesPromocionalesWeb.frx":A1C3
         Top             =   -30
         Width           =   480
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12555
         TabIndex        =   2
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Im�genes Web"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   75
         Width           =   5295
      End
   End
   Begin MSComDlg.CommonDialog ImagePicker 
      Left            =   12120
      Top             =   480
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      DialogTitle     =   "Buscar Imagenes"
   End
   Begin MSComctlLib.TabStrip Categorias 
      Height          =   4815
      Left            =   120
      TabIndex        =   10
      Top             =   4320
      Width           =   15015
      _ExtentX        =   26485
      _ExtentY        =   8493
      MultiRow        =   -1  'True
      HotTracking     =   -1  'True
      Separators      =   -1  'True
      TabMinWidth     =   1411
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Image TemaSeleccionado 
      Height          =   780
      Left            =   13800
      Stretch         =   -1  'True
      Top             =   1680
      Width           =   975
   End
   Begin VB.Image Tema 
      Height          =   420
      Index           =   4
      Left            =   11400
      Picture         =   "FrmImagenesPromocionalesWeb.frx":BF45
      Stretch         =   -1  'True
      Top             =   480
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Image Tema 
      Height          =   420
      Index           =   3
      Left            =   10800
      Picture         =   "FrmImagenesPromocionalesWeb.frx":D066
      Stretch         =   -1  'True
      Top             =   480
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Image Tema 
      Height          =   420
      Index           =   2
      Left            =   10200
      Picture         =   "FrmImagenesPromocionalesWeb.frx":E218
      Stretch         =   -1  'True
      Top             =   480
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Image Tema 
      Height          =   420
      Index           =   1
      Left            =   9600
      Picture         =   "FrmImagenesPromocionalesWeb.frx":E638
      Stretch         =   -1  'True
      Top             =   480
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label lblDescri 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "DESCRIPCI�N"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   240
      Left            =   5040
      TabIndex        =   21
      Top             =   1200
      Width           =   1185
   End
   Begin VB.Label lblLink 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "LINK PARA M�S INFORMACI�N (OPCIONAL)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   240
      Left            =   5040
      TabIndex        =   19
      Top             =   2640
      Width           =   3750
   End
   Begin VB.Label lblImagenPrincipal 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "IMAGEN DE LA SECCI�N"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   240
      Left            =   1335
      TabIndex        =   8
      Top             =   720
      Width           =   2085
   End
   Begin VB.Image Imagen 
      Appearance      =   0  'Flat
      Height          =   2715
      Left            =   240
      Stretch         =   -1  'True
      Top             =   1200
      Width           =   4275
   End
   Begin VB.Label lblClickDerecho 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Click Derecho para eliminar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   240
      Left            =   1215
      TabIndex        =   5
      Top             =   2805
      Width           =   2370
   End
   Begin VB.Label lblDobleClick 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Doble Click para cambiar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   240
      Left            =   1320
      TabIndex        =   4
      Top             =   2445
      Width           =   2130
   End
   Begin VB.Label lblImagen 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Imagen"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   375
      Left            =   1815
      TabIndex        =   3
      Top             =   1965
      Width           =   1080
   End
End
Attribute VB_Name = "FrmImagenesPromocionalesWeb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public fCls As clsProductosWeb

Public WithEvents TmpPreviewPic As Image
Attribute TmpPreviewPic.VB_VarHelpID = -1
Public TmpPreviewPicContainer As PictureBox

Private Secciones As Dictionary
Private Seccion As ClsSeccionWeb
Private Item As ClsImagenWeb

Private Const TmpImgFileBaseName = "StellarProductPicTmp.$(Ext)"
Private TmpImgFileName As String
Private TmpFileNameCollection As Dictionary
Private TmpSelectedIndex As Integer

Private TmpFolder As String, TmpVal As Variant

Private FormaCargada As Boolean
Private CodigoProducto As String

Property Get Producto() As String
    Producto = CodigoProducto
End Property

Property Let Producto(mValor As String)
    CodigoProducto = mValor
End Property

Private Sub FrameTemas_Click()
    LblTemas_Click
End Sub

Private Sub FrameTitulo_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbLeftButton Then MoverVentana Me.hWnd
End Sub

Private Sub FrmEliminar_Click()
    Eliminar_Click
End Sub

Private Sub GRID_DblClick()
    Imagen_DblClick
End Sub

Private Sub grid_EnterCell()
    Grid_Click
End Sub

Private Sub lbl_Organizacion_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    FrameTitulo_MouseMove Button, Shift, x, y
End Sub

Private Sub lbl_website_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    FrameTitulo_MouseMove Button, Shift, x, y
End Sub

Private Sub Eliminar_Click()
    
    Set Seccion = Secciones(Categorias.SelectedItem.Index)
    Set Item = Seccion.Items(Val(Grid.TextMatrix(Grid.Row, 0)))
    
    Item.ImgOriginal = Nothing
    Item.RutaImg = vbNullString
    Item.PoseeCambios = True
    
    If Grid.Rows > 2 Then
        Grid.RemoveItem Grid.Row
        Grid.Row = 1
        Grid_Click
    Else
        Grid.Rows = 1
        NewItemRow
    End If
    
End Sub

Private Sub Grid_Click()

    MousePointer = vbHourglass
    
    If Grid.Row >= 1 Then
        TmpVal = Val(Grid.TextMatrix(Grid.Row, 0))
        If Secciones(Categorias.SelectedItem.Index).Items().Exists(TmpVal) Then
            Set Item = Secciones(Categorias.SelectedItem.Index).Items()(TmpVal)
            txtDescri.Text = Item.Descri
            txtLink = Item.LinkContenido
            If Item.RutaImg <> vbNullString Then
                
                On Error Resume Next
                
                If UCase(Item.Extension) Like UCase("*.png") Then
                    LoadFileToControl PicLoad, Item.RutaImg
                    
                    Set Imagen.Picture = PicLoad.Picture
                Else
                    Imagen.Picture = LoadPicture(Item.RutaImg)
                End If
                
            ElseIf Not Item.ImgOriginal Is Nothing Then
                Set Imagen.Picture = Item.ImgOriginal
            Else
                Set Imagen.Picture = LoadPicture
            End If
        End If
    End If
    
    MousePointer = vbDefault
    
End Sub

Private Sub Agregar_Click()
    NewItemRow
End Sub

Private Sub Categorias_Click()
    IniciarGrid
    CargarGrid (Categorias.SelectedItem.Index)
End Sub

Private Sub IniciarGrid()
    With Me.Grid
        
        .Clear
        .Cols = 2
        .Rows = 2
        .FixedCols = 0
        .FixedRows = 1
        .ScrollBars = flexScrollBarVertical
        EncabezadoGrid 0, "ID", 1750
        .ColAlignment(0) = flexAlignCenterCenter
        EncabezadoGrid 1, "Descripcion", 9200
        .ColAlignment(1) = flexAlignLeftCenter
        
        MinimumTop = 4350
        TmpHighestTop = Categorias.SelectedItem.Top
        
        Dif = CLng(TmpHighestTop) - CLng(MinimumTop)
        
        If Dif > 0 Then
            frmItems.Top = 4800 + Dif
            frmItems.Height = 4100 - Dif
            Grid.Top = 240
            Grid.Height = 3615 - Dif
            'FrmAgregar.Top = 840 + Dif
            'FrmEliminar.Top = 1800 + Dif
        End If
        
    End With
End Sub

Private Sub EncabezadoGrid(Col As Long, Texto As String, Ancho As Long)
    Grid.Row = 0
    Grid.Col = Col
    Grid.Text = Texto
    Grid.ColWidth(Col) = Ancho
    Grid.Font.Bold = True
    'grid.CellAlignment = vbAlignLeft
End Sub

Private Sub NewItemRow()
    
    Set Seccion = Secciones(Categorias.SelectedItem.Index)
    Set Item = New ClsImagenWeb
    Item.Seccion = Seccion
    Item.Orden = Seccion.ProximoID
    Item.Descri = "IMAGEN N� " & Item.Orden
    
    Seccion.Items.Add Item.Orden, Item
    
    Grid.Rows = Grid.Rows + 1
    Grid.Row = Grid.Rows - 1
    Grid.TextMatrix(Grid.Row, 0) = Item.Orden
    Grid.TextMatrix(Grid.Row, 1) = Item.Descri
    
    Grid_Click
    
End Sub

Private Sub CargarGrid(Index As Integer)
    
    Grid.Rows = 1
    
    Set Seccion = Secciones(Index)
    
    For Each Item In AsEnumerable(Seccion.Items.Items())
        Grid.Rows = Grid.Rows + 1
        Grid.Row = Grid.Rows - 1
        Grid.TextMatrix(Grid.Row, 0) = Item.Orden
        Grid.TextMatrix(Grid.Row, 1) = Item.Descri
    Next
    
    If Grid.Rows <= 1 Then NewItemRow
    
    Grid.Row = 1: Grid.Col = 0
    Grid_Click
    
    If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    
End Sub

Private Sub FrmAgregar_Click()
    Agregar_Click
End Sub

Private Function GetFileProp(FilePath As String, Property As String) As Variant
    
    On Error GoTo ErrFileProperties
    
    Dim FSO As New FileSystemObject, TmpFile As File, TmpVar As Variant
    
    Set TmpFile = FSO.GetFile(FilePath)
        
    Select Case UCase(Property)
        Case UCase("Type")
            GetFileProp = TmpFile.Type
        Case UCase("Extension")
            TmpVar = Strings.Split(TmpFile.Path, ".")
            GetFileProp = "." & TmpVar(UBound(TmpVar))
        Case UCase("Description")
            GetFileProp = Null
        Case UCase("Size")
            GetFileProp = TmpFile.Size
    End Select
    
    Exit Function
    
ErrFileProperties:
    
    GetContentType = Null
    
End Function

Private Sub Grabar()
    
    On Error GoTo ErrSave
    
    Dim mRs As Recordset, InitTrans As Boolean, ObjectTemp As Object, DBNull As Variant
    
    Dim TmpSeccion As ClsSeccionWeb, TmpItem As ClsImagenWeb
    
    fCls.ClsCn.Conexion.BeginTrans
    
    InitTrans = True
    
    For Each TmpSeccion In AsEnumerable(Secciones.Items)
        
        For Each TmpItem In AsEnumerable(TmpSeccion.Items.Items())
            
            If TmpItem.PoseeCambios Then
                
                Set mRs = New Recordset
                
                mRs.Open "SELECT * FROM MA_IMAGENES_WEB WHERE Codigo = '" & TmpItem.Codigo & "' AND Orden = " & TmpItem.Orden & " AND Seccion = " & TmpItem.Seccion.Indice, _
                fCls.ClsCn.Conexion, adOpenKeyset, adLockOptimistic, adCmdText
                
                If mRs.EOF Then
                    
                    mRs.AddNew
                    mRs!Codigo = TmpItem.CorrelativoImagen(fCls.ClsCn.Conexion, "Web_Imagen", True)
                    mRs!Orden = TmpItem.Orden
                    mRs!Seccion = TmpItem.Seccion.Indice
                    
                    If TmpItem.RutaImg = vbNullString Then ' Skip
                        mRs.CancelUpdate
                        mRs.Close
                        GoTo Continue
                    End If
                    
                Else
                    mRs!Fecha = fCls.ClsCn.Conexion.Execute("Select GetDate() AS Fecha")!Fecha
                    mRs!cs_Numero_Transferencia = vbNullString
                End If
                
                mRs!UsuarioStellar = LcCodUsuario
                
                If TmpItem.RutaImg <> vbNullString Then
                    
                    Set ObjectTemp = PopFileStream(TmpItem.RutaImg)
                    
                    If Not ObjectTemp Is Nothing Then
                        mRs("Imagen").AppendChunk (ObjectTemp.Read)
                    End If
                    
                    mRs!MIMEType = GetFileProp(TmpItem.RutaImg, "Type")
                    mRs!Extension = GetFileProp(TmpItem.RutaImg, "Extension")
                    mRs!Descripcion = TmpItem.Descri 'GetFileProp(TmpItem.RutaImg, "Description")
                    mRs!Link = TmpItem.LinkContenido
                    mRs!Bytes = GetFileProp(TmpItem.RutaImg, "Size")
                    
                ElseIf TmpItem.ImgOriginal Is Nothing Then
                    
                    mRs!Imagen = Null
                    mRs!MIMEType = Null
                    mRs!Extension = Null
                    mRs!Descripcion = Null
                    mRs!Link = Null
                    mRs!Bytes = Null
                    
                Else
                    mRs!Descripcion = TmpItem.Descri
                    mRs!Link = TmpItem.LinkContenido
                End If
                
                mRs.Update
                
            End If
            
Continue:
            
            Next
            
    Next
    
    fCls.ClsCn.Conexion.CommitTrans
    
    Exit_Click
    
    Exit Sub
    
ErrSave:
    
    'Resume ' Debug
    
    If InitTrans Then fCls.ClsCn.Conexion.RollbackTrans
    
    fCls.Mensaje "Ha ocurrido un error al guardar los datos. Por favor reporte lo siguiente: " & Err.Description
    
End Sub

Private Sub CmdGrabar_Click()
    Call Grabar
End Sub

Private Sub Form_Unload(Cancel As Integer)
    KillSecure TmpFolder & TmpImgFileName
End Sub

Private Sub LblTemas_Click()

    On Error GoTo ErrTema
    
    frm_SeleccionCombo.lbl_Organizacion.Caption = "Seleccione el Tema para el Sitio Web"
    
    frm_SeleccionCombo.CmbSeleccion.Clear
    
    frm_SeleccionCombo.CmbSeleccion.AddItem "1"
    frm_SeleccionCombo.CmbSeleccion.AddItem "2"
    frm_SeleccionCombo.CmbSeleccion.AddItem "3"
    frm_SeleccionCombo.CmbSeleccion.AddItem "4"
    
    frm_SeleccionCombo.CmbSeleccion.ListIndex = 0
    
    frm_SeleccionCombo.CmdSalir.Caption = "OK"
    
    frm_SeleccionCombo.Show vbModal
    
    TmpSel = Val(frm_SeleccionCombo.CmbSeleccion.Text)
    
    If TmpSel > 0 Then
        TmpSql = "UPDATE MA_REGLASDENEGOCIO SET Valor = '" & TmpSel & "' WHERE Campo = 'Web_TemaSeleccionado'"
        fCls.ClsCn.Conexion.Execute TmpSql
        Set TemaSeleccionado.Picture = Tema(TmpSel).Picture
    End If
    
    Exit Sub
    
ErrTema:
    
    fCls.Mensaje "Ha ocurrido un error: " & Err.Description
    
End Sub

Private Sub TmpPreviewPic_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    
    On Error Resume Next
    
    If Button = vbRightButton Or Button = vbMiddleButton Then
        
        If Item.RutaImg = vbNullString Then
            ShowPic
        Else
            ShellExecute 0, vbNullString, "" & Item.RutaImg & "", vbNullString, vbNullString, 1
        End If
        
        TmpPreviewPic_MouseUp vbLeftButton, Shift, x, y
        
    ElseIf Button = vbLeftButton Then
        Me.Controls.Remove TmpPreviewPic
        Me.Controls.Remove TmpPreviewPicContainer
    End If
    
End Sub

Private Sub ShowPic()

    On Error Resume Next
    
    KillSecure TmpFolder & TmpImgFileName
    
    Dim mSql As String, mRs As Recordset, TmpFS As FileSystemObject
    
    'Set mRs = New Recordset
    
    'mSql = "SELECT Imagen FROM MA_IMAGENES_WEB WHERE Codigo = '" & Item.Codigo & "' AND Orden = " & Item.Orden & " AND Seccion = " & Item.Seccion.Indice
    
    'mRs.Open mSql, fCls.ClsCn.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    'If Not mRs.EOF Then
        
        'If Not IsNull(mRs!Imagen) Then
            
            'Call PopStreamOfDb(Imagen, mRs, "Imagen", TmpFolder & TmpImgFileName)
            
            'If TmpFS.FileExists(TmpFolder & TmpImgFileName) Then
                'ShellExecute 0, vbNullString, "" & TmpFS.GetAbsolutePathName(TmpFolder & TmpImgFileName) & "", vbNullString, vbNullString, 1
            'End If
            
        'End If
        
    'End If
    
    Set TmpFS = New FileSystemObject
    
    If Item.RutaImg <> vbNullString Then
        If TmpFS.FileExists(TmpFolder & TmpImgFileName) Then
            ShellExecute 0, vbNullString, "" & TmpFS.GetAbsolutePathName(TmpFolder & TmpImgFileName) & "", vbNullString, vbNullString, 1
        End If
    ElseIf Not Item.ImgOriginal Is Nothing Then
        SavePicture Item.ImgOriginal, TmpFolder & TmpImgFileName
        If TmpFS.FileExists(TmpFolder & TmpImgFileName) Then
            ShellExecute 0, vbNullString, "" & TmpFS.GetAbsolutePathName(TmpFolder & TmpImgFileName) & "", vbNullString, vbNullString, 1
        End If
    End If
    
End Sub

Private Sub CmdCancelar_Click()
    Exit_Click
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    If Not FormaCargada Then
        Unload Me
    End If
End Sub

Private Sub SafeLoadPicture(pCtl, Optional pPicObj As Object, Optional ByVal pFilePath)
    On Error GoTo ErrLoad
    If IsMissing(pFilePath) Then
        Set pCtl.Picture = pPicObj
    Else
        Set pCtl.Picture = LoadPicture(pFilePath)
    End If
    Exit Sub
ErrLoad:
    fCls.Mensaje Err.Description
End Sub

Private Sub Form_Load()
    
    MousePointer = vbHourglass
    
    FormaCargada = CargarSecciones
    
    If Categorias.Tabs.Count > 0 Then
        For Each TmpItem In Categorias.Tabs ' Para seleccionar 1 sin especificar index, ya que vienen de la base de datos.
            Set Categorias.SelectedItem = TmpItem
            'Categorias_Click ' El Select ya lo hace.
            'Grid_Click
            Exit For
        Next
    End If
    
    KillSecure TmpFolder & TmpImgFileName
    
    CargarTema
    
    MousePointer = vbDefault
    
End Sub

Private Sub CargarTema()
    On Error GoTo ErrTema
        TemaSel = BuscarReglaNegocioStr(fCls.ClsCn.Conexion, "Web_TemaSeleccionado", 1)
        If TemaSel > 0 Then
            Set TemaSeleccionado.Picture = Tema(TemaSel).Picture
        End If
    Exit Sub
ErrTema:
    Debug.Print Err.Description
End Sub

Private Function CargarSecciones() As Boolean
    
    On Error GoTo ErrLoad
    
    Set Secciones = New Dictionary
    
    If Not fCls.ClsCn.ConectarBD Then Exit Function
    
    TmpFolder = GetEnvironmentVariable("ProgramData")
    
    If TmpFolder <> vbNullString Then
        TmpFolder = FindPath(SoftwarePath, NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpFolder)
        CreateFullDirectoryPath TmpFolder
    Else
        TmpFolder = FindPath(vbNullString, NO_SEARCH_MUST_FIND_EXACT_MATCH)
    End If
    
    TmpImgFileName = Replace(TmpImgFileBaseName, "$(Ext)", "jpg")
    
    Dim mRs As Recordset
    
    Set mRs = fCls.ClsCn.Conexion.Execute("SELECT * FROM MA_IMAGENES_WEB_SECCION ORDER BY Codigo")
    
    While Not mRs.EOF
        Set Seccion = New ClsSeccionWeb
        Seccion.Indice = Val(mRs!Codigo)
        Seccion.Descripcion = mRs!NombreClaveStellar
        Secciones.Add Seccion.Indice, Seccion
        mRs.MoveNext
    Wend
    
    Categorias.Tabs.Clear
    
    For Each Seccion In AsEnumerable(Secciones.Items())
        Categorias.Tabs.Add Seccion.Indice, , Seccion.Descripcion
    Next
    
    If Categorias.Tabs.Count <= 0 Then Err.Raise 999, , "No se pudieron cargar las secciones de la p�gina web. Verifique los datos."
    
    For Each Seccion In AsEnumerable(Secciones.Items())
        
        fCls.ClsCn.Conexion.Execute "DELETE FROM MA_IMAGENES_WEB WHERE Imagen IS NULL AND LEN (cs_Numero_Transferencia) > 0"
        
        Set mRs = fCls.ClsCn.Conexion.Execute("SELECT * FROM MA_IMAGENES_WEB WHERE NOT (Imagen IS NULL) AND Seccion = " & Seccion.Indice & " ORDER BY Orden")
        
        While Not mRs.EOF
            
            Set Item = New ClsImagenWeb
            
            Item.Seccion = Seccion
            Item.Codigo = mRs!Codigo
            Item.Orden = mRs!Orden
            
            Item.Extension = mRs!Extension
            
            Set Imagen.Picture = LoadPicture()
            
            Call PopStreamOfDb(Imagen, mRs, "Imagen", TmpFolder & TmpImgFileName)
            
            If UCase(Item.Extension) Like UCase("*.png") Then
                LoadFileToControl PicLoad, TmpFolder & TmpImgFileName
                
                'Set Imagen.Picture = PicLoad.Picture
                SafeLoadPicture Imagen, PicLoad.Picture
            Else
                'Set Imagen.Picture = LoadPicture(TmpFolder & TmpImgFileName)
                SafeLoadPicture Imagen, , TmpFolder & TmpImgFileName
            End If
            
            Item.ImgOriginal = Imagen.Picture
            Item.Bytes = mRs!Bytes
            Item.Descri = mRs!Descripcion
            Item.LinkContenido = mRs!Link
            
            Item.MIMEType = mRs!MIMEType
            
            Seccion.Items.Add Item.Orden, Item
            
            mRs.MoveNext
            
        Wend
        
    Next
    
    CargarSecciones = True
    
    Exit Function
    
ErrLoad:
    
    'Resume ' Debug
    
    fCls.Mensaje "Ha ocurrido un error al cargar los datos. Por favor reporte lo siguiente: " & Err.Description
    
End Function

Private Sub Imagen_DblClick()
    
    On Error GoTo Errores
    
    MousePointer = vbHourglass
    
    ImagePicker.CancelError = True
    
    TmpPreviewPic_MouseUp vbLeftButton, 0, 0, 0
        
    ImagePicker.Filter = "Im�genes (*.jpg;*.png;*.gif;*.bmp;*.wmf;*.ico;*.cur)|*.jpg;*.png;*.gif;*.bmp;*.wmf;*.ico;*.cur"
    
    If Item.RutaImg <> vbNullString Then
        ImagePicker.FileName = Item.RutaImg
    End If
    
    ImagePicker.ShowOpen
    
    If ImagePicker.FileName <> vbNullString Then
        
        Item.RutaImg = ImagePicker.FileName
        Item.Extension = GetFileProp(Item.RutaImg, "Extension")
        Item.PoseeCambios = True
        
        If UCase(Item.Extension) Like UCase("*.png") Then
            LoadFileToControl PicLoad, Item.RutaImg
            
            Set Imagen.Picture = PicLoad.Picture
        Else
            Imagen.Picture = LoadPicture(Item.RutaImg)
        End If
        
    End If
    
Finally:
    
    MousePointer = vbDefault
    
    Exit Sub
    
Errores:
    
    If Err.Number <> 32755 Then
        fCls.Mensaje "Ha ocurrido un error al seleccionar el archivo. Por favor reporte lo siguiente: " & Err.Description
    Else
        ' No pasa nada... este error es un artificio para cuando el usuario cancelo la selecci�n.
    End If
    
    GoTo Finally
    
End Sub

Private Sub Imagen_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbRightButton Then
        If fCls.Mensaje("Si desea eliminar la imagen, presione Aceptar.", True) Then
            Set Imagen.Picture = LoadPicture()
            Item.ImgOriginal = Nothing
            Item.RutaImg = vbNullString
            Item.PoseeCambios = True
        End If
    ElseIf Button = vbLeftButton Then
        If Imagen.Picture.Handle <> 0 Then
            ShowPicturePreviewOnForm Me, Imagen.Picture
        Else
            Imagen_DblClick
        End If
    ElseIf Button = vbMiddleButton Then
        TmpPreviewPic_MouseUp vbRightButton, 0, 0, 0
    End If
End Sub

Private Sub txtDescri_Change()
    Item.Descri = txtDescri.Text
    Item.PoseeCambios = True
    Grid.TextMatrix(Grid.Row, 1) = Item.Descri
End Sub

Private Sub txtLink_Change()
    
    TmpSelStart = txtLink.SelStart
    txtLink.TextRTF = txtLink.Text
    txtLink.SelStart = 0
    txtLink.SelLength = Len(txtLink.Text)
    txtLink.SelColor = vbBlue
    txtLink.SelUnderline = True
    txtLink.SelStart = TmpSelStart
    Item.LinkContenido = txtLink.Text
    Item.PoseeCambios = True
    
End Sub

Private Sub txtLink_DblClick()
    If Trim(txtLink.Text) <> vbNullString Then
        ShellExecute Me.hWnd, "open", IIf(UCase(txtLink.Text) Like UCase("http://*"), txtLink.Text, "http://" & txtLink.Text), "", "", 4
    End If
End Sub
