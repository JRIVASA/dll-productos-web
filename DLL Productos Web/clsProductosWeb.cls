VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsProductosWeb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private mClsCn                                                      As clsConexion
Private Const TblProductosWeb As String = "MA_PRODUCTOS_WEB"
Private Const TblPendProductosWeb As String = "TR_PEND_PRODUCTOS_WEB"

Property Get ClsCn() As clsConexion
    Set ClsCn = mClsCn
End Property

Property Get TablaProductoWeb() As String
    TablaProductoWeb = TblProductosWeb
End Property

Property Get TablaProductoWebPendiente() As String
    TablaProductoWebPendiente = TblPendProductosWeb
End Property

Public Sub IniciarInterfaz(Srv As String, BD As String, UserBD As String, Pwd As String, UsuarioStellar As String, _
Optional CnTO As Integer = 15, Optional CmTo As Integer = 30, Optional Provider As String = "SQLOLEDB.1")
    Set mClsCn = New clsConexion
    mClsCn.IniciarClase Srv, BD, UserBD, Pwd, CnTO, CmTo, Provider
    LcCodUsuario = UsuarioStellar
    Set frmMain.fCls = Me
    frmMain.Show vbModal
    Set frmMain = Nothing
    Set mClsCn = Nothing
End Sub

Public Sub InterfazGraficosWeb(Srv As String, BD As String, UserBD As String, Pwd As String, UsuarioStellar As String, _
Optional CnTO As Integer = 15, Optional CmTo As Integer = 30, Optional Provider As String = "SQLOLEDB.1")
    Set mClsCn = New clsConexion
    mClsCn.IniciarClase Srv, BD, UserBD, Pwd, CnTO, CmTo, Provider
    LcCodUsuario = UsuarioStellar
    Set FrmImagenesPromocionalesWeb.fCls = Me
    FrmImagenesPromocionalesWeb.Show vbModal
    Set FrmImagenesPromocionalesWeb = Nothing
    Set mClsCn = Nothing
End Sub

Public Function Mensaje(Msg As String, Optional ByVal MostrarCancel As Boolean = False) As Boolean
    Dim cMsg As clsMensajeria
    
    Set cMsg = New clsMensajeria
    Mensaje = cMsg.Mensaje(Msg, MostrarCancel)
    Set cMsg = Nothing
End Function

Public Function ObtenerRsProducto(pCn As ADODB.Connection, ByVal pCodigo As String) As ADODB.Recordset
    On Error Resume Next
    Set ObtenerRsProducto = New ADODB.Recordset
    ObtenerRsProducto.Open "SELECT * FROM MA_PRODUCTOS WHERE c_Codigo = '" & pCodigo & "'", pCn, adOpenDynamic, adLockOptimistic, adCmdText
End Function

Public Sub AgregarPendienteProd(pCn As ADODB.Connection, pRs As ADODB.Recordset)
    On Error GoTo Error
    If Not pRs Is Nothing Then
        If Not pRs.EOF Then
            Dim mRs As ADODB.Recordset
            Dim mSql As String
            Dim mField As ADODB.Field
            
            mSql = "Select * from vad10.dbo.tr_pendiente_prod where 1=2"
            Set mRs = New ADODB.Recordset
            mRs.Open mSql, pCn, adOpenDynamic, adLockOptimistic, adCmdText
            mRs.AddNew
                For Each mField In pRs.Fields
                    If StrComp(mField.Name, "id", vbTextCompare) <> 0 Then
                        mRs.Fields(mField.Name).Value = mField.Value
                    End If
                Next
                mRs!TipoCambio = 0
                mRs!hablador = 0
            mRs.Update
        End If
    End If
    Exit Sub
Error:
    Err.Clear
End Sub

Public Sub AgregarPendienteCodigos(pCn As ADODB.Connection, ByVal pCodigo As String)
    
    On Error GoTo Error
        
    Dim mRsCodigos As ADODB.Recordset, mSql As String
    
    Set mRsCodigos = New Recordset
    
    mRsCodigos.Open "SELECT * FROM MA_CODIGOS WHERE c_CodNasa = '" & pCodigo & "'", _
    pCn, adOpenDynamic, adLockOptimistic, adCmdText
    
    If Not mRsCodigos.EOF Then
        While Not mRsCodigos.EOF
            mSql = "Insert into vad10.dbo.TR_PENDIENTE_CODIGO  (c_codigo, c_codnasa, c_descripcion, n_cantidad, nu_intercambio, nu_tipoprecio, TipoCambio) " _
                & "values ('" & mRsCodigos!c_Codigo & "','" & mRsCodigos!c_Codnasa & "','" & mRsCodigos!c_Descripcion & "'," & mRsCodigos!n_Cantidad & "," & mRsCodigos!nu_Intercambio & "," & mRsCodigos!nu_TipoPrecio & ", 0)"
            pCn.Execute mSql
            mRsCodigos.MoveNext
        Wend
    End If
        
    Exit Sub
    
Error:
    
    Err.Clear
    
End Sub
